package com.tfg.laura.botalerta;

import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

public class BotMain {

    public static void StartBot() {

        ApiContextInitializer.init();
        TelegramBotsApi telegramBotsApi = new TelegramBotsApi();
            try {
            telegramBotsApi.registerBot(new BotAppAlerta());
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }

    }

}
