package com.tfg.laura.appalerta.casos_uso;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.widget.Toast;

import com.tfg.laura.appalerta.presentacion.MainActivity;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static android.Manifest.permission.RECORD_AUDIO;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static android.content.ContentValues.TAG;
//import static com.tfg.laura.appalerta.presentacion.MainActivity.solicitarPermiso;

public class CasosUsoMicrofono extends MediaRecorder {

    private MediaRecorder mRecorder;
    private MediaPlayer mPlayer;
    private static final String LOG_TAG = "AudioRecording";
    public static String mFileName = null;

    private static final String TAG = "<< MICRÓFONO >>";

    public static String Random;
    public static List <String> RandomListAudio = new ArrayList<>();
    public int flag_audio;
    public static List <Integer> FlagListAudio = new ArrayList<>();

    private Uri file;
    //private StorageMetadata metadata;

    private Activity actividad;
    private int codigoPermiso;
    private int codigoPermiso2;


    private static final int CALL_PHONE = 0;
    private static final int SEND_SMS = 1;
    private static final int READ_CONTACTS = 2;
    private static final int SOLICITUD_PERMISO_LOCALIZACION = 3;
    private static final int SOLICITUD_PERMISO_GRABACION = 4;
    //private static final int RECORD_AUDIO = 4;
    //private static final int STORE = 5;

    public CasosUsoMicrofono(Activity actividad, int codigoPermiso) {
        this.actividad = actividad;
        this.codigoPermiso = codigoPermiso;

    }


    public boolean hayPermisoGrabacionAlmacenamiento() {
        int result = ActivityCompat.checkSelfPermission(actividad, WRITE_EXTERNAL_STORAGE);
        int result1 = ActivityCompat.checkSelfPermission(actividad, RECORD_AUDIO);
        return result == PackageManager.PERMISSION_GRANTED && result1 == PackageManager.PERMISSION_GRANTED;
    }
    public void RequestPermissions(String justificacion, final int requestCode) {
            new AlertDialog.Builder(actividad)
                    .setTitle("Solicitud de permiso")
                    .setMessage(justificacion)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            ActivityCompat.requestPermissions(actividad, new String[]{RECORD_AUDIO, WRITE_EXTERNAL_STORAGE}, requestCode);
                        }})
                    .show();
    }




    @SuppressLint("MissingPermission")
    public void StartGrabacion() {
        if(mRecorder!=null) {
            PararGrabacion();
        }
        if (hayPermisoGrabacionAlmacenamiento()) {
            //mFileName = Environment.getExternalStorageDirectory().getAbsolutePath()+"/" + UUID.randomUUID().toString()+"_audio_record.3gp";

            Random = UUID.randomUUID().toString();
            RandomListAudio.add(Random);
            Log.e(LOG_TAG, "el numero random es:" + Random);
            flag_audio = 0;
            FlagListAudio.add(flag_audio);
            //mFileName = Environment.getExternalStorageDirectory().getAbsolutePath() + "/AppAlerta/AudioRecording" + "_" + String.valueOf(System.currentTimeMillis()) + ".mp3"; ///habrá que ver si ponerle el nombre que quiera la usuaria o lo que sea
            mFileName = Environment.getExternalStorageDirectory().getAbsolutePath() + "/AppAlerta/AudioRecording_" + Random + ".mp3"; ///habrá que ver si ponerle el nombre que quiera la usuaria o lo que sea

            mRecorder = new MediaRecorder();
            mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
            mRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
            mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
            mRecorder.setOutputFile(mFileName);
            try {
                mRecorder.prepare();
                mRecorder.start();
            } catch (IOException e) {
                Log.e(LOG_TAG, "prepare() failed");
            }
            Log.e(TAG, "Recording Started");
        }else {
            RequestPermissions("Sin el permiso de grabación no puedo empezar a grabar", codigoPermiso);
        }
    }



    @SuppressLint("MissingPermission")
    public void PararGrabacion() {

        mRecorder.stop();
        mRecorder.release();
        mRecorder = null;
        Log.e(TAG, "Recording Stopped");
        //Toast.makeText(actividad, "Recording Stopped", Toast.LENGTH_LONG).show();
    }


    public void permisoConcedido() {
        StartGrabacion();

        //adaptador.notifyDataSetChanged();
    }


}
