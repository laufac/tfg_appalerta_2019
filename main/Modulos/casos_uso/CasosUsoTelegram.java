package com.tfg.laura.appalerta.casos_uso;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.util.Log;

import com.google.android.gms.common.api.GoogleApiClient;
import com.tfg.laura.appalerta.presentacion.MainActivity;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashSet;
import java.util.Set;

public class CasosUsoTelegram extends AsyncTask<Void, Void, Void> {

    public SharedPreferences preferencias_dueña;
    private Activity actividad;
    private CasosUsoLocalizacion localizacion;
    private String ChatID;
    private String Nombre_Intro;
    private String Apellidos_Intro;
    private String Mensaje_usuaria;
    private String Mensaje;
    private String UrlMsj;
    private String UrlUbic;
    private String UrlString;
    private String UrlString2;
    private double longitud;
    private double latitud;
    private String apiToken;
    private String Telegram_type;



    /**Ademas con TELEGRAM tengo que cambiar el formato del texto del mensaje para que siempre ponga nomnbres y apellidos de la persona que lo manda para que la gente del grupo sepa quién es**/

    public CasosUsoTelegram(Activity actividad, CasosUsoLocalizacion localizacion ) {
        this.actividad = actividad;
        this.localizacion = localizacion;
    }

    protected Void doInBackground(Void... voids) {

        preferencias_dueña = PreferenceManager.getDefaultSharedPreferences(actividad);
        apiToken = "729249020:AAF030BYgtDCzmhg2jGi5srSCfaaerPHACc";

        ChatID = preferencias_dueña.getString("TelegramID", "");
        Mensaje_usuaria = preferencias_dueña.getString("Mensaje_Telegram", "Hola, estoy en una situación de peligro y necesito ayuda");
        if(Mensaje_usuaria.equals("")){
            Mensaje_usuaria = "Hola, estoy en una situación de peligro y necesito ayuda";
        }
        Nombre_Intro = preferencias_dueña.getString("Nombre", "Usuaria de AppAlerta");
        if (Nombre_Intro.equals("")){
            Nombre_Intro="La usuaria de AppAlerta";
        }
        Apellidos_Intro = preferencias_dueña.getString("Apellidos", "");
        Mensaje = Nombre_Intro + " " + Apellidos_Intro + " envía la siguiente alerta: \n" + Mensaje_usuaria;
        UrlMsj = "https://api.telegram.org/bot%s/sendMessage?chat_id=%s&text=%s";
        UrlUbic = "https://api.telegram.org/bot%s/SendLocation?chat_id=%s&latitude=%s&longitude=%s";
        longitud = localizacion.retLongitud();
        latitud = localizacion.retLatitud();


        Telegram_type = preferencias_dueña.getString("Telegram_ubic","Solo mensaje");

        //Opción de querer enviar solo texto o solo ubicación o ambos
        if(Telegram_type.equals("Solo mensaje")){
            UrlString = String.format(UrlMsj, apiToken, ChatID, Mensaje);
        }else if (Telegram_type.equals("Solo ubicación")) {
            UrlString = String.format(UrlUbic, apiToken, ChatID, latitud, longitud);

        }else if (Telegram_type.equals("Mensaje + ubicación")) {
            UrlString = String.format(UrlMsj, apiToken, ChatID, Mensaje);
            UrlString2 =  String.format(UrlUbic, apiToken, ChatID, latitud, longitud);
        }
        ///me falta la opcion de las dos cosas(a ver si existe una manera de que me lo haga de una... con el url deirectamente...)

        try {
            URL url = new URL(UrlString);
            URLConnection conn = url.openConnection();
            InputStream is = new BufferedInputStream(conn.getInputStream());

            //getting text, we can set it to any TextView
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String inputLine = "";
            StringBuilder sb = new StringBuilder();
            while ((inputLine = br.readLine()) != null) {
                sb.append(inputLine);
            }
            //You can set this String to any TextView
            String response = sb.toString();
            if (Telegram_type.equals("Mensaje + ubicación")) {
                try {
                    URL url2 = new URL(UrlString2);
                    URLConnection conn2 = url2.openConnection();
                    InputStream is2 = new BufferedInputStream(conn2.getInputStream());

                    //getting text, we can set it to any TextView
                    BufferedReader br2 = new BufferedReader(new InputStreamReader(is2));
                    String inputLine2 = "";
                    StringBuilder sb2 = new StringBuilder();
                    while ((inputLine2 = br2.readLine()) != null) {
                        sb2.append(inputLine2);
                    }
                    String response2 = sb2.toString();

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }


        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}