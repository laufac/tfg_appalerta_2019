package com.tfg.laura.appalerta.casos_uso;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.tfg.laura.appalerta.modelo.GeoPunto;
import com.tfg.laura.appalerta.presentacion.AdaptadorContactosBD;
import com.tfg.laura.appalerta.presentacion.Aplicacion;
import com.tfg.laura.appalerta.presentacion.MainActivity;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Locale;

import static android.content.Context.LOCATION_SERVICE;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

import static com.tfg.laura.appalerta.presentacion.MainActivity.solicitarPermiso;

//quitar lo de abstract
public class CasosUsoLocalizacion implements LocationListener {
    private static final String TAG = "AlertaApp_Localización";
    private Activity actividad;
    private int codigoPermiso;
    private LocationManager manejadorLoc;
    private Location mejorLoc;
    private GeoPunto posicionActual;
    private AdaptadorContactosBD adaptador;
    private static final long DOS_MINUTOS = 2 * 60 * 1000;
    private static final int CALL_PHONE = 0;
    private static final int SEND_SMS = 1;
    private static final int READ_CONTACTS = 2;
    private static final int SOLICITUD_PERMISO_LOCALIZACION = 3;
    private String ubicacion;


    /**
     * La variable manejadorLoc nos permite acceder a los servicios de localización de Android.
     * La variable mejorLoc, de tipo Location, almacena la mejor localización actual. La variable
     * posicionActual almacena la misma información, pero en formato GeoPunto. Estará almacenada en
     * Aplicacion para que sea accesible desde cualquier parte de la aplicación. Es necesario tener
     * la información en dos formatos, ya que la primera variable es usada para disponer de la fecha
     * de obtención o proveedor que nos la ha dado y la segunda al ser el formato usado en el resto
     * de la aplicación. Finalmente obtenemos una referencia al adaptador del RecyclerView para poder
     * actualizarlo cuando haya cambios de localización.
     **/

    public CasosUsoLocalizacion(Activity actividad, int codigoPermiso, String ubicacion) {
        this.actividad = actividad;
        this.codigoPermiso = codigoPermiso;
        this.ubicacion = ubicacion;

        manejadorLoc = (LocationManager) actividad.getSystemService(LOCATION_SERVICE);
        posicionActual = ((Aplicacion) actividad.getApplication()).posicionActual;
        //adaptador = ((Aplicacion) actividad.getApplication()).adaptador;
        ultimaLocalizacion();


    }

    //Comprobamos permisos
    /*public boolean hayPermisoLocalizacion() {
        return (ActivityCompat.checkSelfPermission(
                actividad, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED);
    }*/

    public boolean hayPermisoLocalizacionAlmacenamiento() {
        int result = ActivityCompat.checkSelfPermission(actividad, WRITE_EXTERNAL_STORAGE);
        int result1 = ActivityCompat.checkSelfPermission(actividad, ACCESS_FINE_LOCATION);
        return result == PackageManager.PERMISSION_GRANTED && result1 == PackageManager.PERMISSION_GRANTED;
    }

    public void RequestPermissions(String justificacion, final int requestCode) {
        new AlertDialog.Builder(actividad)
                .setTitle("Solicitud de permiso")
                .setMessage(justificacion)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        ActivityCompat.requestPermissions(actividad, new String[]{ACCESS_FINE_LOCATION, WRITE_EXTERNAL_STORAGE}, requestCode);
                    }})
                .show();
    }


    @SuppressLint("MissingPermission")
    public void ultimaLocalizacion() {
        if (hayPermisoLocalizacionAlmacenamiento()) {
            if (manejadorLoc.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                actualizaMejorLocaliz(manejadorLoc.getLastKnownLocation(LocationManager.GPS_PROVIDER));
            }
            if (manejadorLoc.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                actualizaMejorLocaliz(manejadorLoc.getLastKnownLocation(LocationManager.NETWORK_PROVIDER));
            }
        } else {
            RequestPermissions("Sin el permiso de localización no puedo acceder a tu ubicación ni almacenarla en tu dispositivo", codigoPermiso);
            /*solicitarPermiso(Manifest.permission.ACCESS_FINE_LOCATION,
                    "Sin el permiso de localización no puedo mostrar acceder a tu ubicación", codigoPermiso, actividad);*/
        }
    }

    /*public static void solicitarPermiso(final String permiso, String justificacion, final int requestCode, final Activity actividad) {
        if (ActivityCompat.shouldShowRequestPermissionRationale(actividad, permiso)) {
            new AlertDialog.Builder(actividad)
                    .setTitle("Solicitud de permiso")
                    .setMessage(justificacion)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            ActivityCompat.requestPermissions(actividad, new String[]{permiso}, requestCode);
                        }
                    })
                    .show();
        } else {
            ActivityCompat.requestPermissions(actividad, new String[]{permiso}, requestCode);
        }
    }*/

    public void permisoConcedido() {
        ultimaLocalizacion();
        activarProveedores();
        //adaptador.notifyDataSetChanged();
    }

    @SuppressLint("MissingPermission")
    public void activarProveedores() {
        if (hayPermisoLocalizacionAlmacenamiento()) {
            if (manejadorLoc.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                manejadorLoc.requestLocationUpdates(LocationManager.GPS_PROVIDER, 20 * 1000, 5, this);
            }
            if (manejadorLoc.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                manejadorLoc.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 10 * 1000, 10, this);
            }
        } else {
            RequestPermissions("Sin el permiso de localización no puedo acceder a tu ubicación ni almacenarla en tu dispositivo", codigoPermiso);
            /*solicitarPermiso(Manifest.permission.ACCESS_FINE_LOCATION,
                    "Sin el permiso de localización no puedo mostrar acceder a tu ubicación.", codigoPermiso, actividad);*/
        }
    }


    @Override
    public void onLocationChanged(Location location) {
        Log.d(TAG, "Nueva localización: " + location);
        if (hayPermisoLocalizacionAlmacenamiento()) {
            writeToLocationFile("Localizacion", location);
            /**Cada vez que actualiza guardar coordenadaas en archivo de texto que a su vez irá subiendo a drive??**/
        }else{
            RequestPermissions("Sin el permiso de localización no puedo acceder a tu ubicación ni almacenarla en tu dispositivo", codigoPermiso);
        }
        actualizaMejorLocaliz(location);
        //adaptador.notifyDataSetChanged();
    }


    public void writeToLocationFile(String fileName, Location location) {

        //FileOutputStream fos = null;

        BufferedWriter bw = null;
        FileWriter fw = null;

        double longitud = location.getLongitude();
        double latitud = location.getLatitude();
        double altitud = location.getAltitude();
        double accuracy = location.getAccuracy();
        double time = location.getTime();
        String proveedor = location.getProvider();
        double velocidad = location.getSpeed();
        String localizacion = "Longitud: " + longitud + ", Latitud: " + latitud + ", Altitud: " + altitud + ", Precisión: " + accuracy + ", Hora: " + time + ", Proveedor: " + proveedor + ", Velocidad: " + velocidad + "\n";

        final File dir = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/AppAlerta/");
        if (!dir.exists()) {
            if (!dir.mkdirs()) {
                Log.e("ALERT_localizacion", "U AR A MORON!  could not create the directories. CHECK THE FUCKING PERMISSIONS SON!");
            }
        }
        final File myFile = new File(dir, fileName + /*"_" + String.valueOf(System.currentTimeMillis()) + */".txt");

        /**Para que no sobreescriba el archivo usa la clase FileWritter y habilita la opción append (agregar).**/
        /*** PARAMETROS:
         * fileName -> String con el nombre de archivo dependiente del sistema.
         Append -> boolean si es true, entonces los datos se escribirán al final del archivo en lugar del comienzo.**/
        /**FileWriter fw = new FileWriter(myFile.getAbsoluteFile(), true); //opción append habilitada!**/


        try {
            //String data = "Hola stackoverflow.com...";
            //File file = new File("archivo.txt");
            // Si el archivo no existe, se crea!*/
            if (!myFile.exists()) {
                myFile.createNewFile();
            }
            // flag true, indica adjuntar información al archivo.
            fw = new FileWriter(myFile.getAbsoluteFile(), true);
            bw = new BufferedWriter(fw);
            bw.write(localizacion);
            //System.out.println("información agregada!");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                //Cierra instancias de FileWriter y BufferedWriter
                if (bw != null)
                    bw.close();
                if (fw != null)
                    fw.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }


    @Override
    public void onProviderDisabled(String proveedor) {
        Log.d(TAG, "Se deshabilita: " + proveedor);
        activarProveedores();
    }

    @Override
    public void onProviderEnabled(String proveedor) {
        Log.d(TAG, "Se habilita: " + proveedor);
        activarProveedores();
    }

    @Override
    public void onStatusChanged(String proveedor, int estado, Bundle extras) {
        Log.d(TAG, "Cambia estado: " + proveedor);
        activarProveedores();
    }


    private void actualizaMejorLocaliz(Location localiz) {
        if (localiz != null && (mejorLoc == null
                || localiz.getAccuracy() < 2 * mejorLoc.getAccuracy()
                || localiz.getTime() - mejorLoc.getTime() > DOS_MINUTOS)) {
            Log.d(TAG, "Nueva mejor localización");
            mejorLoc = localiz;
            ((Aplicacion) actividad.getApplication()).posicionActual.setLatitud(localiz.getLatitude());
            ((Aplicacion) actividad.getApplication()).posicionActual.setLongitud(localiz.getLongitude());
        }
    }

    public void activar() {
        if (hayPermisoLocalizacionAlmacenamiento()) {
            activarProveedores();
        }
    }


    public void desactivar() {
        if (hayPermisoLocalizacionAlmacenamiento()) manejadorLoc.removeUpdates(this);
    }


    /*********para SMS, LO ESTOY UTILIZANDO????**/
    public String setLocation(GeoPunto posicionActual) {
//Obtener la direccion de la calle a partir de la latitud y la longitud
        if (posicionActual.getLatitud() != 0.0 && posicionActual.getLongitud() != 0.0) {
            try {
                Geocoder geocoder = new Geocoder(actividad, Locale.getDefault());
                List<Address> list = geocoder.getFromLocation(
                        posicionActual.getLatitud(), posicionActual.getLongitud(), 1);
                if (!list.isEmpty()) {
                    Address DirCalle = list.get(0);
                    ubicacion ="Mi direccion es: \n" + DirCalle.getAddressLine(0);
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return ubicacion;
    }

    public double retLongitud(){


        double longitud = posicionActual.getLongitud();
        Log.d("PRUEBA TELEGRAM", "ESTA ES LA LONGITUD: " + longitud);
        return longitud;

    }


    public double retLatitud(){
        double latitud = posicionActual.getLatitud();
        Log.d("PRUEBA TELEGRAM", "ESTA ES LA LATITUD: " + latitud);
        return latitud;
    }

}

