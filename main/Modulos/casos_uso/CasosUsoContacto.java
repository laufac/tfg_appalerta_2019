package com.tfg.laura.appalerta.casos_uso;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.telephony.SmsManager;
import android.widget.Toast;

import com.tfg.laura.appalerta.datos.ContactosBD;
import com.tfg.laura.appalerta.modelo.Contacto;
import com.tfg.laura.appalerta.modelo.GeoPunto;
import com.tfg.laura.appalerta.presentacion.AdaptadorContactosBD;
import com.tfg.laura.appalerta.presentacion.Aplicacion;
import com.tfg.laura.appalerta.presentacion.EditarContactoActivity;
import com.tfg.laura.appalerta.presentacion.MainActivity;
import com.tfg.laura.appalerta.presentacion.Servicio_SMS;
import com.tfg.laura.appalerta.presentacion.VerContactoActivity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static com.tfg.laura.appalerta.presentacion.MainActivity.solicitarPermiso;

public class CasosUsoContacto {

    private Activity actividad;
    public MainActivity actividadMain;
    private ContactosBD contactos;

    private AdaptadorContactosBD adaptador;
    private static final int CALL_PHONE = 0;
    private static final int SEND_SMS = 1;
    private static final int READ_CONTACTS = 2;
    private static final int SOLICITUD_PERMISO_LOCALIZACION = 3;
    private String ubicacion;

    private String SMS_ubic;

    private static final String ubic = "Solo ubicación";
    private static final String texto = "Solo mensaje";
    private static final String ubic_texto = "Mensaje + ubicación";

    private String Mensaje_usuaria;
    private String Nombre_Intro;
    private String Apellidos_Intro;

    private String miInfo;


    public CasosUsoContacto(Activity actividad, ContactosBD contactos, AdaptadorContactosBD adaptador) { //DESPUES CUANDO SE METE bbdd
        //public CasosUsoContacto(Activity actividad, Contactos contactos) {
        this.actividad = actividad;
        this.contactos = contactos;
        this.adaptador = adaptador;
    }

    // OPERACIONES BÁSICAS

    //Para mostrarme el contacto con ese id, en caso de pulsar en él me lleva a la oantalla edicioncontacto (todavia sin hacer)
    public void mostrar(int pos) {
        Intent i = new Intent(actividad, VerContactoActivity.class);
        i.putExtra("pos", pos);
        actividad.startActivity(i);
    }

    public void borrar(final int id) {
        contactos.borrar(id);
        /**para actualizar el cursor y notificar al adaptador que los datos han cambiado:**/
        adaptador.setCursor(contactos.extraeCursor());
        adaptador.notifyDataSetChanged();
        actividad.finish();
    }


    public void editar(int pos, int codigoSolicitud) {

        Intent i = new Intent(actividad, EditarContactoActivity.class);
        i.putExtra("pos", pos);
        actividad.startActivityForResult(i, codigoSolicitud);
    }

    public void guardar(int id, Contacto nuevoContacto) {
        contactos.actualiza(id, nuevoContacto);
        //**Asignamos un nuevo cursor al adaptador y le indicamos que los datos han cambiado para
        // * que vuelva a crear las vistas correspondiente del RecyclerView.
        adaptador.setCursor(contactos.extraeCursor());
        adaptador.notifyDataSetChanged();
    }

    public void nuevo() {
        int id = contactos.nuevo();
        Intent i = new Intent(actividad, EditarContactoActivity.class);
        i.putExtra("_id", id);
        actividad.startActivity(i);
    }


    // INTENCIONES

    /**
     * Llama directamente, necesita activar los permisos de acceder al telefono
     **/
    public void llamarTelefono(Contacto contacto) {
        String telf = Long.toString(contacto.getTelefono());

        if (telf.length() > 9) {
            telf = "+" + telf;
            contacto.setTelefono(Long.parseLong(telf));
        }
        if (ContextCompat.checkSelfPermission(actividad, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
            actividad.startActivity(new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + telf)));
        } else {
            solicitarPermiso(Manifest.permission.CALL_PHONE, "Sin el permiso el administrador de llamadas no podrá realizar la llamada.", CALL_PHONE, actividad);
        }
    }

    public String setLocation(GeoPunto posicionActual) {
//Obtener la direccion de la calle a partir de la latitud y la longitud
        if (posicionActual.getLatitud() != 0.0 && posicionActual.getLongitud() != 0.0) {
            try {
                Geocoder geocoder = new Geocoder(actividad, Locale.getDefault());
                List<Address> list = geocoder.getFromLocation(
                        posicionActual.getLatitud(), posicionActual.getLongitud(), 1);
                if (!list.isEmpty()) {
                    Address DirCalle = list.get(0);
                    ubicacion = "Mi direccion es: \n" + DirCalle.getAddressLine(0);
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return ubicacion;
    }

    public void SendSMS(Contacto contacto) {

        SharedPreferences preferencias_dueña = PreferenceManager.getDefaultSharedPreferences(actividad);
        SMS_ubic = preferencias_dueña.getString("SMS_ubic", "Mensaje + ubicación");

        switch (SMS_ubic) {
            case ubic:
                if (ContextCompat.checkSelfPermission(actividad, Manifest.permission.SEND_SMS) == PackageManager.PERMISSION_GRANTED) {
                    String messageUbic = setLocation(((Aplicacion) actividad.getApplication()).posicionActual);
                    String numeroTel2 = Long.toString(contacto.getTelefono());

                    SmsManager sms = SmsManager.getDefault();
                    ArrayList messagePartsUbic = sms.divideMessage(messageUbic);


                    try {
                        sms.sendMultipartTextMessage(numeroTel2, null, messagePartsUbic, null, null);

                        //Toast.makeText(actividad, "Mensaje Enviado!", Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        //Toast.makeText(actividad, "Fallo el envio!", Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                    }
                }else{
                    solicitarPermiso(Manifest.permission.SEND_SMS, "Sin el permiso el administrador no se podrá realizar la llamada.", SEND_SMS, actividad);
                }
                break;
            case texto:
                Mensaje_usuaria = preferencias_dueña.getString("Mensaje", "Hola, estoy en una situación de peligro y necesito ayuda\n");
                if (Mensaje_usuaria.equals("")){
                    Mensaje_usuaria = "Hola, estoy en una situación de peligro y necesito ayuda";
                }
                Nombre_Intro = preferencias_dueña.getString("Nombre", "Usuaria de AppAlerta");
                if (Nombre_Intro.equals("")){
                    Nombre_Intro="La usuaria de AppAlerta";
                }
                Apellidos_Intro = preferencias_dueña.getString("Apellidos", "");
                String msj = Nombre_Intro + " " + Apellidos_Intro + " envía la siguiente alerta: \n" + Mensaje_usuaria;

                String numeroTel = Long.toString(contacto.getTelefono());
                SmsManager sms = SmsManager.getDefault();
                ArrayList messageParts = sms.divideMessage(msj);

                if (ContextCompat.checkSelfPermission(actividad, Manifest.permission.SEND_SMS) == PackageManager.PERMISSION_GRANTED) {
                    sms.sendMultipartTextMessage(numeroTel, null, messageParts, null, null);
                    //Toast.makeText(actividad, "Sent.", Toast.LENGTH_SHORT).show();
                } else {
                    solicitarPermiso(Manifest.permission.SEND_SMS, "Sin el permiso el administrador no se podrá enviar el SMS de mergencia.", SEND_SMS, actividad);
                }
                break;
            case ubic_texto:
                if (ContextCompat.checkSelfPermission(actividad, Manifest.permission.SEND_SMS) == PackageManager.PERMISSION_GRANTED) {
                    Mensaje_usuaria = preferencias_dueña.getString("Mensaje", "Hola, estoy en una situación de peligro y necesito ayuda\n");
                    if (Mensaje_usuaria.equals("")){
                        Mensaje_usuaria = "Hola, estoy en una situación de peligro y necesito ayuda\n";
                    }
                    Nombre_Intro = preferencias_dueña.getString("Nombre", "Usuaria de AppAlerta");
                    if (Nombre_Intro.equals("")){
                        Nombre_Intro="La usuaria de AppAlerta";
                    }
                    Apellidos_Intro = preferencias_dueña.getString("Apellidos", "");

                    String msjTexto = Nombre_Intro + " " + Apellidos_Intro + " envía la siguiente alerta: \n" + Mensaje_usuaria;
                    String messageUbic = setLocation(((Aplicacion) actividad.getApplication()).posicionActual);
                    String numeroTel2 = Long.toString(contacto.getTelefono());
                    String msj2 = msjTexto + " " + messageUbic;

                    SmsManager sms2 = SmsManager.getDefault();
                    ArrayList messageParts2 = sms2.divideMessage(msj2);


                    try {
                        sms2.sendMultipartTextMessage(numeroTel2, null, messageParts2,  null, null);

                        //Toast.makeText(actividad, "Mensaje Enviado!", Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        //Toast.makeText(actividad, "Fallo el envio!", Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                    }
                }else{
                    solicitarPermiso(Manifest.permission.SEND_SMS, "Sin el permiso el administrador no se podrá realizar la llamada.", SEND_SMS, actividad);
                }
                break;
        }
    }
}









