package com.tfg.laura.appalerta.casos_uso;

import android.app.Activity;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.drive.DriveApi;
import com.google.android.gms.drive.DriveFolder;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.Metadata;
import com.google.android.gms.drive.MetadataChangeSet;
import com.google.android.gms.drive.query.Filters;
import com.google.android.gms.drive.query.Query;
import com.google.android.gms.drive.query.SearchableField;
import com.tfg.laura.appalerta.presentacion.MainActivity;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;

import static com.tfg.laura.appalerta.casos_uso.CasosUsoMicrofono.FlagListAudio;
import static com.tfg.laura.appalerta.casos_uso.CasosUsoMicrofono.RandomListAudio;

public class CasosUsoDrive implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    /**DRIVE***/

    private static final String TAG = "<< DRIVE >>";
    protected static final int REQUEST_CODE_RESOLUTION = 1337;
    private String FOLDER_NAME = "AppAlerta";
    private GoogleApiClient mGoogleApiClient;

    //tipos de datos a almacenar
    private String drive;
    private static final String LOCALIZACION = "Ubicación";
    private static final String AUDIO = "Audio grabado";
    private Activity actividad;
    private String Random_audio;


    public CasosUsoDrive(Activity actividad, GoogleApiClient mGoogleApiClient ) {
        this.actividad = actividad;
        this.mGoogleApiClient = mGoogleApiClient;

        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(actividad).addApi(Drive.API)
                    .addScope(Drive.SCOPE_FILE)
                    .addScope(Drive.SCOPE_APPFOLDER) // required for App Folder sample
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .build();
        }
        mGoogleApiClient.connect();
    }


    public void upload_to_drive() {

        Log.e("drive_PRUEBA", "Estamos en la clase drive");
        CompruebaCarpeta();
    }

    public void CompruebaCarpeta() {
        Log.e(TAG, "Ahora comprobamos carpeta1");
        Query query =
                new Query.Builder().addFilter(Filters.and(Filters.eq(SearchableField.TITLE, FOLDER_NAME), Filters.eq(SearchableField.TRASHED, false)))
                        .build();
        Log.e(TAG, "Ahora comprobamos carpeta2");
        com.google.android.gms.drive.Drive.DriveApi.query(mGoogleApiClient, query).setResultCallback(new ResultCallback<DriveApi.MetadataBufferResult>() {
            @Override public void onResult(DriveApi.MetadataBufferResult result) {
                Log.e(TAG, "Ahora comprobamos carpeta3");
                if (!result.getStatus().isSuccess()) {
                    Log.e(TAG, "Cannot create folder in the root.");
                } else {
                    boolean isFound = false;
                    for (Metadata m : result.getMetadataBuffer()) {
                        if (m.getTitle().equals(FOLDER_NAME)) {
                            Log.e(TAG, "Folder exists");
                            isFound = true;
                            DriveId driveId = m.getDriveId();
                            subir_Drive(driveId);
                            break;
                        }
                    }
                    if (!isFound) {
                        Log.i(TAG, "Folder not found; creating it.");
                        MetadataChangeSet changeSet = new MetadataChangeSet.Builder().setTitle(FOLDER_NAME).build();
                        com.google.android.gms.drive.Drive.DriveApi.getRootFolder(mGoogleApiClient)
                                .createFolder(mGoogleApiClient, changeSet)
                                .setResultCallback(new ResultCallback<DriveFolder.DriveFolderResult>() {
                                    @Override public void onResult(DriveFolder.DriveFolderResult result) {
                                        if (!result.getStatus().isSuccess()) {
                                            Log.e(TAG, "Error al intentar crear carpeta");
                                        } else {
                                            Log.i(TAG, "Carpeta creada");
                                            DriveId driveId = result.getDriveFolder().getDriveId();
                                            subir_Drive(driveId);
                                        }
                                    }
                                });
                    }
                }
            }
        });
    }


    public void subir_Drive(final DriveId driveId){
        Log.e("drive_PRUEBA", "Vamos a subir a drive1");
        com.google.android.gms.drive.Drive.DriveApi.newDriveContents(mGoogleApiClient).setResultCallback(new ResultCallback<DriveApi.DriveContentsResult>() {

            @Override
            public void onResult(@NonNull DriveApi.DriveContentsResult driveContentsResult) {
                if (!driveContentsResult.getStatus().isSuccess()) {
                    Log.e(TAG, "Error al intentar crear contenido");
                    return;
                }
                Log.e("drive_PRUEBA", "Vamos a subir a drive2");
                OutputStream outputStream = driveContentsResult.getDriveContents().getOutputStream();
                MetadataChangeSet changeSet = null;
                SharedPreferences preferencias_dueña = PreferenceManager.getDefaultSharedPreferences(actividad);
                drive = preferencias_dueña.getString("drive","Nada");
                Log.e("drive_PRUEBA", "Vamos a subir a drive");
                switch (drive) {
                    case LOCALIZACION:
                        final File PosFile = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/AppAlerta/localizacion.txt");
                        try {
                            FileInputStream fileInputStream = new FileInputStream(PosFile);
                            byte[] buffer = new byte[1024];
                            int bytesRead;
                            while ((bytesRead = fileInputStream.read(buffer)) != -1) {
                                outputStream.write(buffer, 0, bytesRead);
                            }
                        } catch (IOException e1) {
                            Log.i(TAG, "Imposible escribir fichero de localización.");
                        }

                        //changeSet = new MetadataChangeSet.Builder().setTitle(PosFile.getName()).setMimeType("text/plain").setStarred(false).build();
                        changeSet = new MetadataChangeSet.Builder().setTitle(("localizacion_" + String.valueOf(System.currentTimeMillis()) +".txt" )).setMimeType("text/plain").setStarred(false).build();

                        break;
                    case AUDIO:
                        for(int i=0; i<RandomListAudio.size(); i++){
                            if(FlagListAudio.get(i)==0){
                                FlagListAudio.set(i,1);
                                Random_audio = RandomListAudio.get(i);
                                break;
                            }
                        }
                        //Random_audio = RandomListAudio.get(MainActivity.counter_audio);
                        //Log.e("DRIVE_RANDOM_AUDIO", Random_audio);
                        //final File audioFile = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/AppAlerta/AudioRecording.mp3"); //>>>>>> WHAT FILE ?
                        final File audioFile = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/AppAlerta/AudioRecording_" + Random_audio + ".mp3"); //>>>>>> WHAT FILE ?
                        try {
                            FileInputStream fileInputStream = new FileInputStream(audioFile);
                            byte[] buffer = new byte[21000];
                            int bytesRead;
                            while ((bytesRead = fileInputStream.read(buffer)) != -1) {
                                outputStream.write(buffer, 0, bytesRead);
                            }

                        } catch (IOException e1) {
                            Log.i(TAG, "Imposible escribir fichero de audio.");
                        }

                        changeSet = new MetadataChangeSet.Builder().setTitle(audioFile.getName()).setMimeType("audio/MP3").setStarred(true).build();
                        //changeSet = new MetadataChangeSet.Builder().setTitle(("Audio_" + String.valueOf(System.currentTimeMillis()) +".mp3" )).setMimeType("audio/mp3").setStarred(true).build();

                        break;
                }

                DriveFolder folder = driveId.asDriveFolder();
                folder.createFile(mGoogleApiClient, changeSet, driveContentsResult.getDriveContents())
                        .setResultCallback(new ResultCallback<DriveFolder.DriveFileResult>() {
                            @Override
                            public void onResult(@NonNull DriveFolder.DriveFileResult driveFileResult) {
                                if (!driveFileResult.getStatus().isSuccess()) {
                                    Log.e(TAG, "U AR A MORON!  Error while trying to create the file");
                                    return;
                                }
                                Log.v(TAG, "Created a file: " + driveFileResult.getDriveFile().getDriveId());
                                mGoogleApiClient.disconnect();
                            }
                        });
            }
        });
    }


    @Override public void onConnected(@Nullable Bundle bundle) {
        Log.v(TAG, "+++++++++++++++++++ onConnected +++++++++++++++++++");
        //actividad = MainActivity.instanceMain;
    }

    @Override public void onConnectionSuspended(int i) {
        Log.e(TAG, "onConnectionSuspended [" + String.valueOf(i) + "]");
    }

    @Override public void onConnectionFailed(@NonNull ConnectionResult result) {
        Log.i(TAG, "GoogleApiClient connection failed: " + result.toString());
        if (!result.hasResolution()) {
            // show the localized error dialog.
            GoogleApiAvailability.getInstance().getErrorDialog(actividad, result.getErrorCode(), 0).show();
            return;
        }
        try {
            result.startResolutionForResult(actividad, REQUEST_CODE_RESOLUTION);
        } catch (IntentSender.SendIntentException e) {
            Log.e(TAG, "U AR A MORON! Exception while starting resolution activity", e);
        }
    }
}
