package com.tfg.laura.appalerta.modelo;

public class Contacto {

    private String nombre;
    private String contacto_id;
    private String estado;
    private long telefono;
    private long fecha;



    public Contacto(String nombre, String estado, String contacto_id, long telefono) {
        this.nombre = nombre;
        this.estado = estado;
        this.contacto_id = contacto_id;
        this.telefono = telefono;
        this.fecha = System.currentTimeMillis();
    }



    public Contacto() {
        this.nombre = "";
        estado = "Nuevo";
        contacto_id = "";
        fecha = System.currentTimeMillis();
    }


    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }


    public String getContacto_id() {
        return contacto_id;
    }

    public void setContacto_id(String contacto_id) {
        this.contacto_id = contacto_id;
    }

    public void setTelefono(long telefono) {
        this.telefono = telefono;
    }

    public long getTelefono() {
        return telefono;
    }

    public void setFecha(long fecha) {
        this.fecha = fecha;
    }

    public long getFecha() {
        return fecha;
    }

    @Override
    public String toString() {
        return "Contacto{" +
                "nombre='" + nombre + '\'' +
                "estado='" + estado + '\'' +
                ", telefono=" + telefono +
                ", fecha=" + fecha +
                '}';
    }
}
