package com.tfg.laura.appalerta.presentacion;

import android.database.Cursor;

import com.tfg.laura.appalerta.datos.Contactos;
import com.tfg.laura.appalerta.datos.ContactosBD;
import com.tfg.laura.appalerta.modelo.Contacto;

public class AdaptadorContactosBD extends AdaptadorContactos {

    protected Cursor cursor;

    /**
     * nuevo parámetro de tipo Cursor, que es el resultado de una consulta en la base de datos.
     * Realmente es aquí donde vamos a buscar los elementos a listar en el RecyclerView. Esta forma
     * de trabajar es mucho más versátil que utilizar un array, podemos listar un tipo de lugar o que
     * cumplan una determinada condición sin más que realizar un pequeño cambio en la consulta SQL.
     * Además, podremos ordenarlos por valoración o cualquier otro criterio, porque se mostrarán en
     * el mismo orden como aparecen en el Cursor. Por otra parte, resulta muy eficiente dado que se
     * realiza solo una consulta a la base de datos, dejando el resultado almacenado en la variable
     * de tipo Cursor.
     **/

    public AdaptadorContactosBD(Contactos contactos, Cursor cursor) {
        super(contactos);
        this.cursor = cursor;
    }

    public Cursor getCursor() {
        return cursor;
    }

    public void setCursor(Cursor cursor) {
        this.cursor = cursor;
    }

    /**
     * vamos a poder acceder a un lugar, indicar su posición en Cursor o, lo que es lo mismo, su
     * posición en el listado. Para ello, movemos el cursor a la posición indicada y extraemos el
     * lugar en esta posición, utilizando un método estático de LugarBD.
     **/
    public Contacto contactoPosicion(int posicion) {
        cursor.moveToPosition(posicion);
        return ContactosBD.obtieneContacto(cursor);
    }

    //realizar una operación de borrado o edición de un registro de la base de datos, vamos a identificar
    // el lugar a modificar por medio de la columna _id.
    public int idPosicion(int posicion) {
        cursor.moveToPosition(posicion);
        return cursor.getInt(0);
    }


    //Los dos últimos métodos ya existen en la clase que extendemos, pero los vamos a reemplazar;
    // por esta razón tienen la etiqueta de override.

    /**
     * se utilizaba para personalizar la vista ViewHolder en una determinada posición.
     * La gran diferencia entre el nuevo método es que ahora el lugar lo obtenemos del cursor,
     * mientras que en el método anterior se obtenía de lugares. Esto supondría una nueva consulta
     * en la base de datos por cada llamada a onBindViewHolder(), lo que sería muy poco eficiente.
     * En el método getItemCount() pasa algo similar, obtener el número de elementos directamente
     * del cursor es más eficiente que hacer una nueva consulta.
     **/
    @Override
    public void onBindViewHolder(ViewHolder holder, int posicion) {
        Contacto contacto = contactoPosicion(posicion);
        holder.personaliza(contacto);
        holder.itemView.setTag(new Integer(posicion)); //El atributo Tag permite asociar a una vista cualquier objeto con información extra. La idea es asociar a cada vista del RecyclerView la posición que ocupa en el listado. La ideas es que cuando asociamos un onClickListener este nos indica la vista pulsada, pero no la posición. De esta forma, sabiendo la vista conoceremos su posición.
    }

    @Override
    public int getItemCount() {
        return cursor.getCount();
    }


    /**se recorren todos los elementos del adaptador hasta encontrar uno con el mismo id. Si no es
     * encontrado devolvemos -1.**/
    public int posicionId(int id) {
        int pos = 0;
        while ((pos < getItemCount()) && (idPosicion(pos) != id)) {
            pos++;
            return -1;
        }
        return pos;
    }

    /*
    public int posicionId(int id) {
        int pos = 0;
        while (pos = getItemCount()) return -1;
    else                       return pos;
    }*/


}


