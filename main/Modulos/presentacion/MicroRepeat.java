package com.tfg.laura.appalerta.presentacion;

import android.content.Intent;

import java.util.TimerTask;

public class MicroRepeat extends TimerTask {
    public int estado;

    public MicroRepeat(int estado ) {
        this.estado = estado;
    }

    public void run(){
        new Background_Microfono(estado).execute();
    }

}
