package com.tfg.laura.appalerta.presentacion;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;

import com.tfg.laura.appalerta.casos_uso.CasosUsoContacto;
import com.tfg.laura.appalerta.casos_uso.CasosUsoMicrofono;
import com.tfg.laura.appalerta.datos.ContactosBD;
import com.tfg.laura.appalerta.modelo.Contacto;

public class Background_SMS extends AsyncTask<Void, Void, Void> {

    public MainActivity actividad;
    private Contacto contacto;
    private ContactosBD contactos;
    private CasosUsoContacto UsoContacto;
    private int pos;
    private AdaptadorContactosBD adaptador;
    private static final int CALL_PHONE = 0;
    private static final int SEND_SMS = 1;
    private static final int READ_CONTACTS = 2;
    private static final int SOLICITUD_PERMISO_LOCALIZACION = 3;
    private Handler mHandler;
    private String tipo;

    private int _id;


    public Background_SMS(int pos, ContactosBD contactos, AdaptadorContactosBD adaptador ) {
        this.pos = pos;
        this.contactos = contactos;
        this.adaptador = adaptador;
    }

    protected Void doInBackground(Void... voids) {
        //Bundle extras = intent.getExtras();

        actividad = MainActivity.instanceMain;

        UsoContacto = new CasosUsoContacto( actividad , contactos, adaptador);

        contacto = adaptador.contactoPosicion(pos); //que me devuelve el lugar en esa posición(id)
        //if (extras != null) pos = extras.getInt("pos", 0);
        //else pos = 0;

        SharedPreferences preferencias_dueña = PreferenceManager.getDefaultSharedPreferences(actividad);
        tipo = preferencias_dueña.getString("SMS_todos", "Contacto principal");


        if (tipo.equals("Contacto principal")){
            UsoContacto.SendSMS(contacto);
        }else if (tipo.equals("Todos")) {
            for (int i = 0; i < contactos.size(); i++){
                Contacto contacto_actual = adaptador.contactoPosicion(i);
                UsoContacto.SendSMS(contacto_actual);
            }
        }
        return null;
    }
}

