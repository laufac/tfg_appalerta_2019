package com.tfg.laura.appalerta.presentacion;

import android.Manifest;
import android.app.Activity;
import android.app.IntentService;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.tfg.laura.appalerta.casos_uso.CasosUsoContacto;
import com.tfg.laura.appalerta.datos.ContactosBD;
import com.tfg.laura.appalerta.modelo.Contacto;

import java.util.ArrayList;

import static com.tfg.laura.appalerta.presentacion.MainActivity.solicitarPermiso;

public class Servicio_SMS extends IntentService {


    private View vista;
    private Contacto contacto;
    private ContactosBD contactos;
    private CasosUsoContacto UsoContacto;
    private int pos;
    private AdaptadorContactosBD adaptador;
    public MainActivity actividad;
    private static final int CALL_PHONE = 0;
    private static final int SEND_SMS = 1;
    private static final int READ_CONTACTS = 2;
    private static final int SOLICITUD_PERMISO_LOCALIZACION = 3;
    private Handler mHandler;
    private String tipo;

    private int _id;

    public Servicio_SMS() {
        super("Servicio SMS");
        //this.actividad = actividad;
    }


    @Override
    public void onCreate() {
        super.onCreate();
        mHandler = new Handler();
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        Bundle extras = intent.getExtras();

        actividad = MainActivity.instanceMain;

        pos = extras.getInt("pos", 0); //se averigua la posición del lugar a mostrar, que ha sido pasado en un extra.
        tipo = extras.getString("Tipo", "SMS");
        contactos = ((Aplicacion) getApplication()).contactos; //A partir de este id obtenemos el objeto Lugar a mostrar.
        adaptador = ((Aplicacion) getApplication()).adaptador;
        UsoContacto = new CasosUsoContacto( actividad , contactos, adaptador);

        contacto = adaptador.contactoPosicion(pos); //que me devuelve el lugar en esa posición(id)
        if (extras != null) pos = extras.getInt("pos", 0);
        else pos = 0;

        SharedPreferences preferencias_dueña = PreferenceManager.getDefaultSharedPreferences(actividad);
        tipo = preferencias_dueña.getString("SMS_todos", "Contacto principal");


        if (tipo.equals("Contacto principal")){
            UsoContacto.SendSMS(contacto);
        }else if (tipo.equals("Todos")) {
            for (int i = 0; i < contactos.size(); i++){
                Contacto contacto_actual = adaptador.contactoPosicion(i);
                UsoContacto.SendSMS(contacto_actual);
            }
        }
    }
}