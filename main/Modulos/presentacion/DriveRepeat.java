package com.tfg.laura.appalerta.presentacion;

import android.app.Activity;

import com.google.android.gms.common.api.GoogleApiClient;
import com.tfg.laura.appalerta.casos_uso.CasosUsoDrive;

import java.util.TimerTask;

public class DriveRepeat  extends TimerTask {
    public GoogleApiClient mGoogleApiClient;
    public Activity actividad;

    public DriveRepeat(Activity actividad, GoogleApiClient mGoogleApiClient ) {
        this.actividad = actividad;
        this.mGoogleApiClient = mGoogleApiClient;
    }

    public void run(){
        new Background_Drive(actividad,mGoogleApiClient).execute();
    }
}
