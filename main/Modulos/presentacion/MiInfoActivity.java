package com.tfg.laura.appalerta.presentacion;


import android.os.Bundle;
import android.preference.PreferenceActivity;

import com.tfg.laura.appalerta.R;

public class MiInfoActivity extends PreferenceActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.info_pref);
    }
}







