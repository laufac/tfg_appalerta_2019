package com.tfg.laura.appalerta.presentacion;

import android.app.Activity;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tfg.laura.appalerta.R;
import com.tfg.laura.appalerta.casos_uso.CasosUsoContacto;
import com.tfg.laura.appalerta.datos.Contactos;
import com.tfg.laura.appalerta.datos.ContactosBD;

public class ContactosActivity extends AppCompatActivity {

    private CasosUsoContacto usoContacto;
    private int pos;
    private RecyclerView recyclerView;
    private ContactosBD contactos;
    public AdaptadorContactosBD adaptador;

    //private LugaresBD lugares;
    //private AdaptadorLugaresBD adaptador;



    @Override public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contactos_emergencia);

        contactos = ((Aplicacion) getApplication()).contactos;
        adaptador = ((Aplicacion) getApplication()).adaptador;
        usoContacto = new CasosUsoContacto(this, contactos, adaptador); //CUANDO METAMOS BBDD
        //usoContacto = new CasosUsoContacto(this, contactos);
        //Cada vez que queramos ejecutar este cado de uso:
        //usoLugar.mostrar(pos);


        /*declaramos recyclerView y lo inicializamos con findViewById().*/
        recyclerView = findViewById(R.id.recycler_view);
        /*indicamos que las vistas a mostrar serán de tamaño fijo y que usaremos un LayoutManager de tipo LinearLayoutManager.*/
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adaptador);


        /***Creo que esto es para que vaya al vista lugar al hacer clicj en el???**/

        adaptador.setOnItemClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //int pos = recyclerView.getChildAdapterPosition(v); // nos indicará la posición de una vista dentro del adaptador.
                int pos =(Integer)(v.getTag());
                usoContacto.mostrar(pos);
            }
        });


        /**Tengo que investigar cómo volver a sacar la toolbar y el sobre bien!**/

      /*Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);*/


        /**Para que al pulsar en el mas cree un nuevo contacto de 0**/

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /***TODAVIA DEBO DEFINIR ESTE METODO!!!!**/
                usoContacto.nuevo();
            }
        });
    }
}
