package com.tfg.laura.appalerta.presentacion;

import android.app.IntentService;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.view.View;

import com.tfg.laura.appalerta.casos_uso.CasosUsoContacto;
import com.tfg.laura.appalerta.casos_uso.CasosUsoMicrofono;
import com.tfg.laura.appalerta.datos.ContactosBD;
import com.tfg.laura.appalerta.modelo.Contacto;

public class Servicio_Micro extends IntentService {


    private static final int SOLICITUD_PERMISO_GRABACION = 4;
    private CasosUsoMicrofono usoMicrofono;
    public MainActivity actividad;
    private Handler mHandler;

    public Servicio_Micro() {
        super("Servicio SMS");
        //this.actividad = actividad;
    }


    @Override
    public void onCreate() {
        super.onCreate();
        mHandler = new Handler();
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        Bundle extras = intent.getExtras();

        actividad = MainActivity.instanceMain;

        actividad = MainActivity.instanceMain;
        usoMicrofono = new CasosUsoMicrofono(actividad, SOLICITUD_PERMISO_GRABACION);

        usoMicrofono.StartGrabacion();

    }
}