package com.tfg.laura.appalerta.presentacion;

import android.app.Activity;
import android.app.Application;

import com.google.android.gms.maps.model.LatLng;
import com.tfg.laura.appalerta.datos.ContactosBD;
import com.tfg.laura.appalerta.modelo.GeoPunto;


public class Aplicacion extends Application {

    //public Contactos contactos = new ListaContactos();
    //public AdaptadorContactos adaptador = new AdaptadorContactos(contactos);

    public ContactosBD contactos;
    //la lista de lugares que el usuario a buscado en la base de datos estará almacenada dentro de adaptador
    public AdaptadorContactosBD adaptador;

    @Override public void onCreate() {
        super.onCreate();
        contactos = new ContactosBD(this);
        adaptador= new AdaptadorContactosBD(contactos, contactos.extraeCursor());
    }

    //private final LatLng UPV = new LatLng(39.481106, -0.340987);

    public GeoPunto posicionActual = new GeoPunto(39.481106, 0.340987);
    //public GeoPunto posicionActual = new GeoPunto(0.0, 0.0);
    /*public Contactos getContactos() {
        return contactos;
    }*/
}