package com.tfg.laura.appalerta.presentacion;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.tfg.laura.appalerta.R;
import com.tfg.laura.appalerta.datos.Contactos;
import com.tfg.laura.appalerta.modelo.Contacto;

public class AdaptadorContactos extends RecyclerView.Adapter<AdaptadorContactos.ViewHolder> {
    protected Contactos contactos;         // Lista de lugares a mostrar
    protected View.OnClickListener onClickListener;
    final static int RESULTADO_EDITAR = 1;

    /**En el constructor se inicializa el conjunto de datos a mostrar y otras variables globales a la clase. **/
    public AdaptadorContactos(Contactos contactos) {
        this.contactos = contactos;
    }

    public void setOnItemClickListener(View.OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    //Creamos nuestro ViewHolder, con los tipos de elementos (las vistas ) a modificar
    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView nombre;
        public ImageView foto; //Más adelante una foto de la persona, icono si no hay foto???
        public TextView telf;

        public ViewHolder(View itemView) {
            super(itemView);
            nombre = itemView.findViewById(R.id.nombre);
            foto = itemView.findViewById(R.id.foto_contacto);
            telf  = itemView.findViewById(R.id.telefono);

        }
        // Personalizamos un ViewHolder a partir de un contacto
        public void personaliza(Contacto contacto) {
            String telefo;
            nombre.setText(contacto.getNombre());
            telf.setText(Long.toString(contacto.getTelefono()));
            foto.setScaleType(ImageView.ScaleType.FIT_END);
        }
    }

    /**El objeto inflador nos va a permitir crear una vista a partir de su XML.*/
    // Creamos el ViewHolder con la vista de un elemento sin personalizar
    /**El método onCreateViewHolder() devuelve una vista de un elemento sin personalizar. Podríamos
     *  definir diferentes vistas para diferentes tipos de elementos utilizando el parámetro
     *  viewType.Usamos el método inflate() para crear una vista a partir del layout XML definido en
     *  elemento_lista. En este método se indica como segundo parámetro el layout padre que contendrá
     *  a la vista que se va a crear. En este caso, resulta imprescindible indicarlo, ya que queremos
     *  que la vista hijo ha de adaptarse al tamaño del padre (en elemento_lista se ha indicado
     *  layout_width="match_parent").  El tercer parámetro del método permite indicar si queremos
     *  que la vista sea insertada en el padre. Indicamos false, dado que esta operación la va a
     *  hacer el ReciclerView.**/

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // Inflamos la vista desde el xml
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.contacto_individual, parent, false);
        //View v = inflador.inflate(R.layout.elemento_lista, null);
        v.setOnClickListener(onClickListener);
        return new ViewHolder(v);
    }

    // Usando como base el ViewHolder y lo personalizamos
    /**El método onBindViewHolder() personaliza un elemento de tipo ViewHolder según su
     * posicion. A partir del ViewHolder que personalizamos ya es el sistema quien se
     * encarga de crear la vista definitiva que será insertada en el ReciclerView. **/

    @Override
    public void onBindViewHolder(ViewHolder holder, int posicion) {
        Contacto contacto = contactos.contacto(posicion);
        holder.personaliza(contacto);
    }
    // Indicamos el número de elementos a visualizar de la lista
    @Override public int getItemCount() {
        return contactos.size();
    }




}
