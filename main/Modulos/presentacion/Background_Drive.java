package com.tfg.laura.appalerta.presentacion;

import android.app.Activity;
import android.os.AsyncTask;

import com.google.android.gms.common.api.GoogleApiClient;
import com.tfg.laura.appalerta.casos_uso.CasosUsoDrive;


public class Background_Drive extends AsyncTask<Void, Void, Void> {

    private static final int SOLICITUD_PERMISO_GRABACION = 4;
    private CasosUsoDrive usoDrive;
    public Activity actividad;
    private GoogleApiClient mGoogleApiClient;

    public Background_Drive(Activity actividad, GoogleApiClient mGoogleApiClient ) {
        this.actividad = actividad;
        this.mGoogleApiClient = mGoogleApiClient;
    }

    protected Void doInBackground(Void... voids) {
        usoDrive = new CasosUsoDrive(actividad, mGoogleApiClient);
        usoDrive.upload_to_drive();
        return null;
    }
}
