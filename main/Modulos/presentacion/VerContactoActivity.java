package com.tfg.laura.appalerta.presentacion;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.tfg.laura.appalerta.R;
import com.tfg.laura.appalerta.casos_uso.CasosUsoContacto;
import com.tfg.laura.appalerta.datos.ContactosBD;
import com.tfg.laura.appalerta.modelo.Contacto;

import java.text.DateFormat;
import java.util.Date;

public class VerContactoActivity extends AppCompatActivity{


    private static final int CALL_PHONE = 0;

    private CasosUsoContacto usoContacto;
    private int pos;
    private Contacto contacto;
    final static int RESULTADO_EDITAR = 1;
    //final static int RESULTADO_GALERIA = 2;
    //final static int RESULTADO_FOTO = 3;
    //private ImageView foto;
    //private Uri uriUltimaFoto;
    //private static final int READ_EXTERNAL_STORAGE = 0;
    private View vista;
    private ContactosBD contactos;
    private AdaptadorContactosBD adaptador; //???
    public int _id = -1;

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ver_contacto);
        Bundle extras = getIntent().getExtras();


        pos = extras.getInt("pos", 0); //se averigua la posición del lugar a mostrar, que ha sido pasado en un extra.
        contactos = ((Aplicacion) getApplication()).contactos; //A partir de este id obtenemos el objeto Lugar a mostrar.
        adaptador = ((Aplicacion) getApplication()).adaptador;
        usoContacto = new CasosUsoContacto(this, contactos, adaptador);
        //lugar = lugares.elemento(pos);
        //adaptador = ((Aplicacion) getApplication()).getAdaptador();
        contacto =  adaptador.contactoPosicion (pos); //que me devuelve el lugar en esa posición(id)
        if (extras != null) pos = extras.getInt("pos", 0);
        else                pos = 0;
        _id = adaptador.idPosicion(pos);
        //actualizaVistas(pos);
        actualizaVista();


    }

    @Override public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_contacto, menu);
        return true;
    }

    @Override public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.editar_contacto:
                contacto.setEstado("En edición");
                usoContacto.editar(pos, RESULTADO_EDITAR);
                return true;
            case R.id.borrar_contacto:
                if(contactos.size() == 1){
                    new AlertDialog.Builder(this)
                            .setMessage("Debes tener al menos un contacto en tu lista de contactos de emergencia")
                            .show();
                }else {
                    new AlertDialog.Builder(this)
                            .setTitle("Borrar de contacto")
                            .setMessage("¿Estás seguro de que quieres eliminar este contacto?")
                            .setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    int _id = adaptador.idPosicion(pos);
                                    //usoLugar.borrar(pos);
                                    usoContacto.borrar(_id);
                                }
                            })
                            .setNegativeButton("Cancelar", null)
                            .show();
                    actualizaVista();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        /*if (requestCode == RESULTADO_EDITAR) {
            actualizaVistas();
            findViewById(R.id.scrollView1).invalidate();*/
        if(requestCode == RESULTADO_EDITAR) {
            //pos = -1;

            contacto =  adaptador.contactoPosicion (pos); //que me devuelve el lugar en esa posición(id)
            _id = adaptador.idPosicion(pos);


            /*contacto = contactos.contacto(_id);
            pos = adaptador.posicionId(_id); // buscamos la nueva posición a partir de _id.*/
            actualizaVista();  ///que me coja las nuevas cosaaas
        }
    }


    public void actualizaVista() {
        contacto = adaptador.contactoPosicion (pos);
        TextView nombre = findViewById(R.id.nombre);
        nombre.setText(contacto.getNombre());
        TextView nombre_contacto = findViewById(R.id.nombre_contacto);
        nombre_contacto.setText(contacto.getNombre()); // + " " + contacto.getApellido());
        TextView telefono = findViewById(R.id.telefono);
        telefono.setText(Long.toString(contacto.getTelefono()));
        TextView fecha = findViewById(R.id.fecha);
        fecha.setText(DateFormat.getDateInstance().format(
                new Date(contacto.getFecha())));
    }

    //Intenciones implícitas
    //Metodos de los onClick de vista_lugar.xml
    /*

    public void ponerDeGaleria(View view) {
        usoLugar.ponerDeGaleria(RESULTADO_GALERIA);
    }
    public void tomarFoto(View view) {
        uriUltimaFoto = usoLugar.tomarFoto(RESULTADO_FOTO);
    }
    public void eliminarFoto(View view) {
        usoLugar.ponerFoto(pos, "", foto);
    }


    @Override public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == CALL_PHONE) {
            if (grantResults.length== 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                llamarTelefono(vista);
            } else {
                Toast.makeText(this, "Sin el permiso, no puedo realizar la " +
                        "acción", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
*/


}

