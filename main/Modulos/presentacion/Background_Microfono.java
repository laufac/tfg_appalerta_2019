package com.tfg.laura.appalerta.presentacion;

import android.os.AsyncTask;

import com.tfg.laura.appalerta.casos_uso.CasosUsoMicrofono;

public class Background_Microfono extends AsyncTask<Void, Void, Void> {

    private static final int SOLICITUD_PERMISO_GRABACION = 4;
    private CasosUsoMicrofono usoMicrofono;
    public MainActivity actividad;
    public int estado;

    public Background_Microfono(int estado ) {
        this.estado = estado;
    }

    protected Void doInBackground(Void... voids) {
        actividad = MainActivity.instanceMain;
        usoMicrofono = new CasosUsoMicrofono(actividad, SOLICITUD_PERMISO_GRABACION);

        if(estado == 0) {
            usoMicrofono.StartGrabacion();
        }else{
            usoMicrofono.PararGrabacion();
        }
        return null;
    }
}
