package com.tfg.laura.appalerta.presentacion;

import android.Manifest;
import android.app.Activity;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Spinner;

import com.tfg.laura.appalerta.R;
import com.tfg.laura.appalerta.casos_uso.CasosUsoContacto;
import com.tfg.laura.appalerta.datos.ContactosBD;
import com.tfg.laura.appalerta.modelo.Contacto;

import java.util.ArrayList;
import java.util.List;

import static com.tfg.laura.appalerta.presentacion.MainActivity.solicitarPermiso;

//import static com.tfg.laura.appalerta.presentacion.MainActivity.solicitarPermiso;


public class EditarContactoActivity extends AppCompatActivity {

    private EditText nombre;
    private EditText telefono;
    private Button BuscarContacto;
    static final int PICK_CONTACT = 1;
    String st;
    String contacto_id;
    private static int _PermisoObtenido;
    private int flag_tlf_nuevo;

    private Activity actividad;

    final private int REQUEST_MULTIPLE_PERMISSIONS = 124;

    private static final int CALL_PHONE = 0;
    private static final int SEND_SMS = 1;
    private static final int READ_CONTACTS = 2;
    private static final int SOLICITUD_PERMISO_LOCALIZACION = 3;

    private CasosUsoContacto usoContacto;
    private int pos;
    private Contacto contacto;
    private Contacto nuevoContacto = new Contacto();
    private ContactosBD contactos;
    private AdaptadorContactosBD adaptador;

    private int _id;

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edicion_contacto);
        contactos = ((Aplicacion) getApplication()).contactos; //A partir de este id obtenemos el objeto Lugar a mostrar.
        adaptador = ((Aplicacion) getApplication()).adaptador;
        Bundle extras = getIntent().getExtras();
        pos = extras.getInt("pos", -1); //se averigua la posición del lugar a mostrar, que ha sido pasado en un extra.
        _id = extras.getInt("_id",-1);
        if (_id!=-1) contacto = contactos.contacto(_id);
        else contacto =  adaptador.contactoPosicion (pos);


        /*pos = extras.getInt("pos", 0); //se averigua la posición del lugar a mostrar, que ha sido pasado en un extra.
        contacto =  adaptador.contactoPosicion (pos); //que me devuelve el lugar en esa posición(id)
        if (extras != null) pos = extras.getInt("pos", 0);
        else                pos = 0;
        _id = adaptador.idPosicion(pos);*/

        //AccessContact();
        BuscarContacto = findViewById(R.id.btnBuscarContacto);
        nombre = findViewById(R.id.nombre);
        telefono= findViewById(R.id.telefono);
        //actualizaVista();

        BuscarContacto.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                ComprobarPermisos();
                /*
                if(hayPermisoContactos()){
                    AccessContact();
                }else{
                    SolicitarPermiso();
                }*/
            }
        });

        usoContacto = new CasosUsoContacto(this, contactos, adaptador);
        actualizaVista();

    }

    @Override public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_edicion, menu);
        return true;
    }

    @Override public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.accion_cancelar:
                if (_id != -1) contactos.borrar(_id);
                finish();
                return true;
            case R.id.accion_guardar:
                //actualizaVista();
                if (_id == -1) {
                    _id = adaptador.idPosicion(pos); //La variable pos corresponde a un indicador de posición dentro de la lista. Para utilizar correctamente el método actualiza() de LugaresBD, hemos de obtener el _id correspondiente a la primera columna de la tabla. Este cambio lo realiza el método idPosicion() de adaptador.
                    if((contacto.getNombre()!=(nombre.getText().toString()))||(contacto.getNombre()!=(nombre.getText().toString()))) {

                        if (CompruebaRepetidos() != 1) {
                            String telf = telefono.getText().toString();
                            telf = telf.replace(" ", "");
                            if (telf.length() > 9) {
                                if (telf.substring(0, 2).equals("34")) {  //movil español
                                    telf = telf.substring(2);
                                } else if (telf.substring(0, 3).equals("+34")) {
                                    telf = telf.substring(3);
                                }
                            }
                            contacto.setNombre(nombre.getText().toString());
                            contacto.setEstado("Editado");
                            contacto.setContacto_id(contacto_id);
                            contacto.setFecha(System.currentTimeMillis());
                            contacto.setTelefono(Long.parseLong(telf));
                            //usoLugar.guardar(pos, nuevoLugar);
                            usoContacto.guardar(_id, contacto);
                            finish();
                        } else {
                            new AlertDialog.Builder(this)
                                    .setTitle("Contacto repetido")
                                    .setMessage("Este contacto ya está en tu lista")
                                    .show();
                        }
                    }
                } else if (contacto_id == null) { //caso en el que el contacto no se toma de la lista si no que se escribe directamente
                    if ((nombre.getText().toString().equals("")) || (telefono.getText().toString().equals("0"))) { //caso en el que el contacto no se toma de la lista si no que se escribe directamente
                        //contactos.borrar(_id);
                        nuevoContacto.setEstado("Nuevo");
                        new AlertDialog.Builder(this)
                                .setTitle("Contacto Vacío")
                                .setMessage("Debes introducir los datos del contacto o buscarlo en tu lista de contactos")
                                .show();
                    }else if (CompruebaRepes() == 1){
                        new AlertDialog.Builder(this)
                                .setTitle("Contacto repetido")
                                .setMessage("Este contacto ya está en tu lista")
                                .show();

                    } else {
                        NuevoContacto();
                        finish();
                    }
                } else if (CompruebaRepetidos() == 1) {
                    new AlertDialog.Builder(this)
                            .setTitle("Contacto repetido")
                            .setMessage("Este contacto ya está en tu lista")
                            .show();


                } else if ((nombre.equals(null)) || (telefono == null)) { //caso de contacto vacio
                    contactos.borrar(_id);
                    new AlertDialog.Builder(this)
                            .setTitle("Contacto Vacío")
                            .setMessage("Debes introducir los datos del contacto o buscarlo en tu lista de contactos")
                            .show();
                }else {
                    String telf = telefono.getText().toString();
                    telf = telf.replace(" ", "");
                    if (telf.length() > 9){
                        if (telf.substring(0, 2).equals("34")) {  //movil español
                            telf = telf.substring(2);
                        } else if (telf.substring(0, 3).equals("+34")) {
                            telf = telf.substring(3);
                        }
                    }
                    nuevoContacto.setNombre(nombre.getText().toString());
                    nuevoContacto.setEstado("Nuevo");
                    nuevoContacto.setContacto_id(contacto_id);
                    nuevoContacto.setFecha(System.currentTimeMillis());
                    nuevoContacto.setTelefono(Long.parseLong(telf));
                    //usoLugar.guardar(pos, nuevoLugar);
                    usoContacto.guardar(_id, nuevoContacto);
                    //actualizaVistas();
                    finish();
                }

                return true;
            default:
                contactos.borrar(_id);
                return super.onOptionsItemSelected(item);
        }
    }

    public void actualizaVista() {
        nombre = findViewById(R.id.nombre);
        nombre.setText(contacto.getNombre());
        nuevoContacto.setContacto_id(contacto_id);
        telefono= findViewById(R.id.telefono);
        if(contacto.getTelefono() != 0) {
            telefono.setText(Long.toString(contacto.getTelefono()));
        }
    }


    //Accediendo a contactos

    public void ComprobarPermisos(){
        if(hayPermisoContactos()){
            AccessContact();
        }else{
            SolicitarPermiso();
        }
    }

    public boolean hayPermisoContactos(){
        return (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED);
    }

    public static void PermisoObtenido(int permisoobtenido){
        _PermisoObtenido = permisoobtenido;
        /*if(permisoobtenido == 1) {
            AccessContact();
        }*/
        /*}else {
            ComprobarPermisos();  ///NO SE SI ESTO ME GENERA UN BUCLE!
        }*/
    }

    public void SolicitarPermiso(){
        solicitarPermiso(Manifest.permission.READ_CONTACTS, "La aplicación necesita tu permiso para acceder a tu agenda.", READ_CONTACTS, this);
        if(_PermisoObtenido == 1){
            AccessContact();
        }
    }


    /*public static void solicitarPermiso(final String permiso, String justificacion, final int requestCode, final Activity actividad) {
        if (ActivityCompat.shouldShowRequestPermissionRationale(actividad, permiso)){
            new AlertDialog.Builder(actividad)
                    .setTitle("Solicitud de permiso")
                    .setMessage(justificacion)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            ActivityCompat.requestPermissions(actividad, new String[]{permiso}, requestCode);
                        }})
                    .show();
        } else {
            ActivityCompat.requestPermissions(actividad, new String[]{permiso}, requestCode);
        }
    }*/

    public void AccessContact() {
        Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
        startActivityForResult(intent, PICK_CONTACT);
    }

    public void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);
        switch (reqCode) {
            case (PICK_CONTACT):
                if (resultCode == Activity.RESULT_OK) {
                    Uri contactData = data.getData();
                    Cursor c = managedQuery(contactData, null, null, null, null);
                    if (c.moveToFirst()) {
                        String id = c.getString(c.getColumnIndexOrThrow(ContactsContract.Contacts._ID));
                        String hasPhone = c.getString(c.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));
                        try {
                            if (hasPhone.equalsIgnoreCase("1")) {
                                Cursor phones = getContentResolver().query(
                                        ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                                        ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + id,
                                        null, null);
                                phones.moveToFirst();
                                contacto_id = id;
                                String cNumber = phones.getString(phones.getColumnIndex("data1"));
                                //System.out.println("number is:" + cNumber);
                                //contacto_id.setText(contact_id);
                                telefono.setText(cNumber);
                                //telefono.setText("Phone Number is: "+cNumber);
                            }
                            String name = c.getString(c.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                            nombre.setText(name);
                        } catch (Exception ex) {
                            st = "Error";
                            st.toString();
                        }
                    }
                }
                break;
        }
    }

    public int CompruebaRepetidos(){ //busca repetido si contacto tomado de la agenda
        String telf = telefono.getText().toString();
        if(contactos.BuscaContactoId(contacto_id) == true){
            return 1;
        }else{
            return 0;
        }

    }

    public int CompruebaRepes(){ //busca repetidos de los añadidos manualmente
        String telf = telefono.getText().toString();
        if(contactos.BuscaContactotelf(telf) == true){
            return 1;
        }else{
            return 0;
        }

    }

    @Override
    public void onBackPressed() {
        if(contacto.getEstado().equals("Nuevo")) {
            contactos.borrar(_id);
        }
        super.onBackPressed();
    }


    public void NuevoContacto(){
        String telf = telefono.getText().toString();
        telf = telf.replace(" ", "");
        if (telf.length() > 9){
            if (telf.substring(0, 2).equals("34")) {  //movil español
                telf = telf.substring(2);
            } else if (telf.substring(0, 3).equals("+34")) {
                telf = telf.substring(3);
            }
        }
        nuevoContacto.setNombre(nombre.getText().toString());
        nuevoContacto.setEstado("Nuevo");
        nuevoContacto.setContacto_id(contacto_id);
        nuevoContacto.setFecha(System.currentTimeMillis());
        nuevoContacto.setTelefono(Long.parseLong(telf));
        //usoLugar.guardar(pos, nuevoLugar);
        usoContacto.guardar(_id, nuevoContacto);
        //actualizaVistas();
    }
}








