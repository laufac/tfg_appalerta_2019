
package com.tfg.laura.appalerta.presentacion;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.Fragment;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.tfg.laura.appalerta.R;
import com.tfg.laura.appalerta.casos_uso.CasosUsoLocalizacion;
import com.tfg.laura.appalerta.modelo.GeoPunto;

public class MapaActivity extends FragmentActivity
        implements OnMapReadyCallback, GoogleMap.OnInfoWindowClickListener {
    private GoogleMap mapa; //Nos permitirá acceder al objeto GoogleMap que hemos insertado en un fragment de nuestro layout.
    private GeoPunto miPosicion;

    /**adaptaremos la actividad MapaActivity para que use adecuadamente el nuevo adaptador basado
     * en Cursor. El proceso es el mismo que acabamos de realizar: Reemplazaremos los accesos a
     * Aplicacion.lugares por Aplicacion.adaptador.**/

    @Override public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mapa);
        miPosicion = ((Aplicacion) getApplication()).posicionActual;
        //miPosicion = CasosUsoLocalizacion.ultimaLocalizazion();
        //lugares = ((Aplicacion) getApplication()).lugares;
        //adaptador = ((Aplicacion) getApplication()).adaptador;
        SupportMapFragment mapFragment = (SupportMapFragment)
                getSupportFragmentManager().findFragmentById(R.id.mapa);
        mapFragment.getMapAsync(this);
    }

    /**El mapa es cargado asíncronamiente, por lo que es necesario implementar el método onMapReady
     * de la interfaz OnMapReadyCallback, que sera llamado en el momento en que mapa esté listo.
     * Será en este método cuándo podremos configurar el objeto GoogleMap que nos pasan como
     * parámetro, para adaptarlo a nuestras necesidades.**/
    @Override public void onMapReady(GoogleMap googleMap) {
        //miPosicion = CasosUsoLocalizacion.ultimaLocalizazion();
        mapa = googleMap;
        mapa.setMapType(GoogleMap.MAP_TYPE_NORMAL); //permite seleccionar el tipo de mapa (normal, satélite, hibrido o relieve).
        if (ActivityCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION) ==
                PackageManager.PERMISSION_GRANTED) {
            mapa.setMyLocationEnabled(true);
            mapa.getUiSettings().setZoomControlsEnabled(true);//HABILITADO ZOOM
            mapa.getUiSettings().setCompassEnabled(true);//VISIBLE BRUJULA
        }
        /**Queremos utilizar los iconos utilizados en la aplicación. El problema es que su
         *  tamaño es excesivo. Para resolverlo los leemos como Drawables de los recursos,
         *  los convertimos en Bitmap y los escalamos dividiendo su anchura y altura entre siete.**/
                /*Bitmap iGrande = BitmapFactory.decodeResource(
                        getResources(), lugar.getTipo().getRecurso());
                Bitmap icono = Bitmap.createScaledBitmap(iGrande,
                        iGrande.getWidth() / 7, iGrande.getHeight() / 7, false);*/
        mapa.addMarker(new MarkerOptions()
                .position(new LatLng(miPosicion.getLatitud(), miPosicion.getLongitud()))
                .title("Estás aquí"));
        //.icon(BitmapDescriptorFactory.fromBitmap(icono)));///  YA VERE QUE ICONO LE PONGO
        //   }
        //}

        mapa.setOnInfoWindowClickListener(this);
    }

    /**Se llamará a este método cuando se pulse sobre cualquier ventana de información.
     * Para averiguar el marcador al que corresponde, se pasa el objeto Marker que se ha pulsado.
     * En el marcador hemos introducido alguna información sobre el lugar (como el nombre);
     * sin embargo, lo que necesitamos es el id del lugar. No resulta sencillo introducir este id
     * en un objeto Marker. Para resolverlo hemos introducido un bucle donde se busca un lugar cuyo
     * nombre coincida con el título de marcador. Cuando se encuentre una coincidencia, se creará
     * una intención para lanzar la actividad correspondiente.**/
    @Override
    public void onInfoWindowClick(Marker marker) {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}

