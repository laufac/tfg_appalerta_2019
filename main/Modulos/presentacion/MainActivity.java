package com.tfg.laura.appalerta.presentacion;


import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Build;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.util.Size;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.drive.CreateFileActivityOptions;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.drive.DriveApi;
import com.google.android.gms.drive.DriveClient;
import com.google.android.gms.drive.DriveContents;
import com.google.android.gms.drive.DriveFile;
import com.google.android.gms.drive.DriveFolder;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.DriveResourceClient;
import com.google.android.gms.drive.Metadata;
import com.google.android.gms.drive.MetadataChangeSet;
import com.google.android.gms.drive.query.Filters;
import com.google.android.gms.drive.query.Query;
import com.google.android.gms.drive.query.SearchableField;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;
import com.tfg.laura.appalerta.R;
import com.tfg.laura.appalerta.casos_uso.CasosUsoContacto;
import com.tfg.laura.appalerta.casos_uso.CasosUsoDrive;
import com.tfg.laura.appalerta.casos_uso.CasosUsoLocalizacion;
import com.tfg.laura.appalerta.casos_uso.CasosUsoMicrofono;
import com.tfg.laura.appalerta.casos_uso.CasosUsoTelegram;
import com.tfg.laura.appalerta.datos.ContactosBD;
import com.tfg.laura.appalerta.modelo.Contacto;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.ref.WeakReference;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.Timer;

import static android.widget.Toast.makeText;
import static com.tfg.laura.appalerta.casos_uso.CasosUsoMicrofono.mFileName;


public class MainActivity extends AppCompatActivity /*implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener*/ {

    public static MainActivity instanceMain;


    /**BOTONES**/
    private ImageButton bContactos;
    private ImageButton bPref;
    private ImageButton bPanico;
    //private Button bContactos;
    //private Button bPanico;

    private int ContactIsNull = 0;

    private ContactosBD contactos;
    private Contacto contacto;
    private CasosUsoContacto UsoContacto;
    private CasosUsoLocalizacion usoLocalizacion;
    private CasosUsoMicrofono usoMicrofono;
    private AdaptadorContactosBD adaptador;
    private int pos;
    //public int _id;
    public int PermisoObtenido = 0;
    public int _id = -1;

    /**PERMISOS**/
    private static final int CALL_PHONE = 0;
    private static final int SEND_SMS = 1;
    private static final int READ_CONTACTS = 2;
    private static final int SOLICITUD_PERMISO_LOCALIZACION = 3;
    private static final int SOLICITUD_PERMISO_GRABACION = 4;

    /**PREFERENCIAS**/

    private static final String TAG_Panico = "<< Pánico Lanzado >>";

    public SharedPreferences preferencias_dueña;

    //KEYS

    private static String Nombre = "";
    private static String Apellidos = "";
    private static Set Medios_default;
    private static Set Medios;
    private static String SMS_tipo;
    private static String SMS_ubic;
    private static String Mensaje_SMS = "";
    private static String Telegram_type;
    private static String ChatID = "";
    private static String Mensaje_Telegram = "";
    private static boolean micro = false;
    private static String drive;


    private View vista;


    private static final int RECOGNIZE_SPEECH_ACTIVITY = 3;
    private int Voz;

    /**DRIVE**/

    //private int DrivePulsado;

    private static final String TAG = "<< DRIVE >>";
    protected static final int REQUEST_CODE_RESOLUTION = 1337;
    private String FOLDER_NAME = "AppAlerta";
    public static GoogleApiClient mGoogleApiClient;
    //tipos de datos a almacenar
    private static final int LOCALIZACION = 000;
    private static final int AUDIO = 001;

    private CasosUsoDrive usoDrive;
    private Timer timer;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //para darle color al fondo
        // Now get a handle to any View contained
        // within the main layout you are using
        //View someView = findViewById(R.id.btnPanico);

        // Find the root view
        //View root = someView.getRootView();

        // Set the color
        //root.setBackgroundColor(getResources().getColor(android.R.color.holo_purple));


        instanceMain = this;

        contactos = ((Aplicacion) getApplication()).contactos; //A partir de este id obtenemos el objeto contacto
        adaptador = ((Aplicacion) getApplication()).adaptador;
        UsoContacto = new CasosUsoContacto(this, contactos, adaptador);

        /**Por ahora pos= 0**** Esto variará según preferencias*/
        pos = 0;
        contacto = adaptador.contactoPosicion(pos); //que me devuelve el lugar en esa posición(id) ///por defecto el primero... habrá que ver si puedo hacer un bucle con todos o buscar uno específico
        if (ContactIsNull(contacto, pos) == false) {
            _id = adaptador.idPosicion(pos);
        }

        usoLocalizacion = new CasosUsoLocalizacion(this, SOLICITUD_PERMISO_LOCALIZACION, "");
        usoMicrofono = new CasosUsoMicrofono(this, SOLICITUD_PERMISO_GRABACION);
        usoDrive = new CasosUsoDrive(this,mGoogleApiClient );
        timer = new Timer();

        preferencias_dueña = PreferenceManager.getDefaultSharedPreferences(this);
        initGoogleAssistant();

        /**BOTONES**/
        bPref = findViewById(R.id.btnPreferencias);
        bPref.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                lanzarInfo(null);
            }
        });

        bContactos = findViewById(R.id.btnContactos);
        bContactos.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                lanzarContactos(null);
            }
        });

        bPanico = findViewById(R.id.btnPanico);
        bPanico.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (preferencias_dueña.getBoolean("Por_voz", false)== false) {
                    lanzarPanico(null);
                }
            }
        });

    }


    /**
     * MENU
     **/
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.instrucciones) {
            lanzarInstrucciones(null);
            return true;
        }
        if (id == R.id.acercaDe) {
            lanzarAcercaDe(null);
            return true;
        }
        if (id == R.id.menu_mapa) {
            //usoLocalizacion = new CasosUsoLocalizacion(this, SOLICITUD_PERMISO_LOCALIZACION);
            Intent intent = new Intent(this, MapaActivity.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }


    /**
     * Actividades Menú
     **/
    public void lanzarAcercaDe(View view) {
        Intent i = new Intent(this, AcercaDeActivity.class);
        startActivity(i);
    }

    public void lanzarInfo(View view) {
        Intent i = new Intent(this, MiInfoActivity.class);
        startActivity(i);
    }

    public void lanzarInstrucciones(View view) {
        Intent i = new Intent(this, Instrucciones_Activity.class);
        startActivity(i);
    }

    public void lanzarPantallaPanico(View view){
        Intent i = new Intent(this, PanicoLanzadoActivity.class);
        startActivity(i);
    }


    /**
     * Actividades botones
     **/
    public void lanzarContactos(View view) {
        Intent i = new Intent(this, ContactosActivity.class);
        startActivity(i);
    }

    public void lanzarPanico(View view) {  ////tengo que adaptar este boton a las preferencias del usuario!! siempre comprobando el length de contactos!! para no intentar acceder a un numero de contacto que no hay
        //Antes de nada hay que comprobar que en la lista de contactos haya alguno definido
        pos = 0;
        //int _id = adaptador.idPosicion(pos);

        contacto = adaptador.contactoPosicion(pos);

        //LO PRIMERO COMPROBAMOS QUE MEDIOS HAY SELECCIONADOS EN PREFERENCIAS:

        Medios = preferencias_dueña.getStringSet("Medios", Collections.<String>emptySet());

        SMS_tipo = preferencias_dueña.getString("SMS_todos", "Contacto principal");
        SMS_ubic = preferencias_dueña.getString("SMS_ubic", "Mensaje + ubicación");
        Mensaje_SMS = preferencias_dueña.getString("Mensaje_SMS", "Hola, estoy en una situación de peligro y necesito ayuda");
        Telegram_type = preferencias_dueña.getString("Telegram_ubic", "Solo mensaje");
        ChatID = preferencias_dueña.getString("TelegramID", "");
        Mensaje_Telegram = preferencias_dueña.getString("Mensaje_Telegram", "Hola, estoy en una situación de peligro y necesito ayuda");
        micro = preferencias_dueña.getBoolean("microfono", false);
        drive = preferencias_dueña.getString("drive", "Nada");

        Toast.makeText(this, "PANICO LANZADO", Toast.LENGTH_SHORT).show();

        if (Medios.isEmpty()) {
            new AlertDialog.Builder(this)
                    .setTitle("Selecciona un medio")
                    .setMessage("No has seleccionado ningún medio de contacto en las 'Preferencias' a la hora de pulsar el botón del pánico Para más información pulsa en 'Acerca de...'")
                    .show();
        }else if((micro==false)&&(drive.equals("Audio grabado"))){
            new AlertDialog.Builder(this)
                    .setTitle("Micrófono no activado")
                    .setMessage("Si quieres que el archivo de audio se suba a tu cuenta de drive, debes marcar la opción micrófono en la pantalla de 'Preferencias'. Para más información pulsa en 'Acerca de...'")
                    .show();
        }else if(Medios.contains("Llamada")){
            if (ContactIsNull(contacto, pos) == false) {
                if(!(Medios.contains("SMS"))&& !(Medios.contains("Telegram"))){
                    if((micro==false)&&(drive.equals("Nada"))) { /**SOLO LLAMADA**/
                        Log.e(TAG_Panico, "SOLO LLAMADA");
                        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
                            lanzarPantallaPanico(null);
                            UsoContacto.llamarTelefono(contacto);
                        } else {
                            solicitarPermiso(Manifest.permission.CALL_PHONE, "Sin el permiso el administrador de llamadas no podrá realizar la llamada.", CALL_PHONE, this);
                        }
                    }else if ((micro==true)&&(drive.equals("Nada"))){  /**Llamada + Micro**/
                        Log.e(TAG_Panico, "Llamada + Micro");
                        if((ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) && (usoMicrofono.hayPermisoGrabacionAlmacenamiento())){
                            lanzarPantallaPanico(null);
                            UsoContacto.llamarTelefono(contacto);
                            int start = 0;
                            int stop = 1;
                            timer.schedule(new MicroRepeat(start), 0, 29000);
                        }else {
                            if (ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                solicitarPermiso(Manifest.permission.CALL_PHONE, "Sin el permiso el administrador de llamadas no podrá realizar la llamada.", CALL_PHONE, this);
                            }
                            if (!usoMicrofono.hayPermisoGrabacionAlmacenamiento()) {
                                usoMicrofono.RequestPermissions("Sin el permiso de grabación no puedo empezar a grabar", SOLICITUD_PERMISO_GRABACION);
                            }
                        }
                    }else if ((micro==false)&&(!drive.equals("Nada"))){  /**Llamada + Drive**/
                        Log.e(TAG_Panico, "Llamada + Drive");
                        if((ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) && (usoLocalizacion.hayPermisoLocalizacionAlmacenamiento())){
                            lanzarPantallaPanico(null);
                            UsoContacto.llamarTelefono(contacto);
                            //new CasosUsoDrive(this, mGoogleApiClient).execute();
                            timer.schedule(new DriveRepeat(this, mGoogleApiClient), 0, 30000);
                        }else {
                            if (ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                solicitarPermiso(Manifest.permission.CALL_PHONE, "Sin el permiso el administrador de llamadas no podrá realizar la llamada.", CALL_PHONE, this);
                            }
                            if (!usoLocalizacion.hayPermisoLocalizacionAlmacenamiento()) {
                                usoLocalizacion.RequestPermissions("Sin el permiso de grabación no puedo acceder a su ubicación y alamacenarla", SOLICITUD_PERMISO_LOCALIZACION);
                            }
                        }
                    }else if ((micro==true)&&(!drive.equals("Nada"))) { /**Llamada + Micro + Drive**/
                        Log.e(TAG_Panico, "Llamada + Micro + Drive");
                        if ((ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) && (usoLocalizacion.hayPermisoLocalizacionAlmacenamiento()) && (usoMicrofono.hayPermisoGrabacionAlmacenamiento())) {
                            lanzarPantallaPanico(null);
                            UsoContacto.llamarTelefono(contacto);
                            int start = 0;
                            int stop = 1;
                            timer.schedule(new MicroRepeat(start), 0, 29000);
                            timer.schedule(new DriveRepeat(this, mGoogleApiClient), 31000, 31000);
                        } else {
                            if (ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                solicitarPermiso(Manifest.permission.CALL_PHONE, "Sin el permiso el administrador de llamadas no podrá realizar la llamada.", CALL_PHONE, this);
                            }
                            if (!usoLocalizacion.hayPermisoLocalizacionAlmacenamiento()) {
                                usoLocalizacion.RequestPermissions("Sin el permiso de grabación no puedo acceder a su ubicación y alamacenarla", SOLICITUD_PERMISO_LOCALIZACION);
                            }
                            if (!usoMicrofono.hayPermisoGrabacionAlmacenamiento()) {
                                usoMicrofono.RequestPermissions("Sin el permiso de grabación no puedo empezar a grabar", SOLICITUD_PERMISO_GRABACION);
                            }
                        }
                    }
                }else if(!(Medios.contains("SMS"))&& (Medios.contains("Telegram"))) {
                    if ((micro == false) && (drive.equals("Nada"))) { /**Llamada + Telegram**/
                        Log.e(TAG_Panico, "Llamada + Telegram");
                        if ((ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED)) {
                            lanzarPantallaPanico(null);
                            UsoContacto.llamarTelefono(contacto);
                            new CasosUsoTelegram(this, usoLocalizacion).execute();
                        } else {
                            if (ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                solicitarPermiso(Manifest.permission.CALL_PHONE, "Sin el permiso el administrador de llamadas no podrá realizar la llamada.", CALL_PHONE, this);
                            }
                        }
                    } else if ((micro == true) && (drive.equals("Nada"))) {  /**Llamada + Telegram + Micro**/
                        Log.e(TAG_Panico, "Llamada + Telegram + Micro");
                        if ((ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) && (usoMicrofono.hayPermisoGrabacionAlmacenamiento())) {
                            lanzarPantallaPanico(null);
                            UsoContacto.llamarTelefono(contacto);
                            new CasosUsoTelegram(this, usoLocalizacion).execute();
                            int start = 0;
                            int stop = 1;
                            timer.schedule(new MicroRepeat(start), 0, 29000);
                        } else {
                            if (ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                solicitarPermiso(Manifest.permission.CALL_PHONE, "Sin el permiso el administrador de llamadas no podrá realizar la llamada.", CALL_PHONE, this);
                            }
                            if (!usoMicrofono.hayPermisoGrabacionAlmacenamiento()) {
                                usoMicrofono.RequestPermissions("Sin el permiso de grabación no puedo empezar a grabar", SOLICITUD_PERMISO_GRABACION);
                            }
                        }
                    } else if ((micro == false) && (!drive.equals("Nada"))) {  /**Llamada + Telegram + Drive**/
                        Log.e(TAG_Panico, "Llamada + Telegram + Drive");
                        if ((ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) && (usoLocalizacion.hayPermisoLocalizacionAlmacenamiento())) {
                            lanzarPantallaPanico(null);
                            UsoContacto.llamarTelefono(contacto);
                            new CasosUsoTelegram(this, usoLocalizacion).execute();
                           // new CasosUsoDrive(this, mGoogleApiClient).execute();
                            timer.schedule(new DriveRepeat(this, mGoogleApiClient), 0, 30000);
                        } else {
                            if (ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                solicitarPermiso(Manifest.permission.CALL_PHONE, "Sin el permiso el administrador de llamadas no podrá realizar la llamada.", CALL_PHONE, this);
                            }
                            if (!usoLocalizacion.hayPermisoLocalizacionAlmacenamiento()) {
                                usoLocalizacion.RequestPermissions("Sin el permiso de grabación no puedo acceder a su ubicación y alamacenarla", SOLICITUD_PERMISO_LOCALIZACION);
                            }
                        }
                    } else if ((micro == true) && (!drive.equals("Nada"))) { /**Llamada + Telegram + Micro + Drive**/
                        Log.e(TAG_Panico, "Llamada + Telegram + Drive + Micro");
                        if ((ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) && (usoMicrofono.hayPermisoGrabacionAlmacenamiento())) {
                            lanzarPantallaPanico(null);
                            UsoContacto.llamarTelefono(contacto);
                            new CasosUsoTelegram(this, usoLocalizacion).execute();
                            int start = 0;
                            int stop = 1;
                            timer.schedule(new MicroRepeat(start), 0, 29000);
                            timer.schedule(new DriveRepeat(this, mGoogleApiClient), 31000, 31000);
                        } else {
                            if (ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                solicitarPermiso(Manifest.permission.CALL_PHONE, "Sin el permiso el administrador de llamadas no podrá realizar la llamada.", CALL_PHONE, this);
                            }
                            if (!usoLocalizacion.hayPermisoLocalizacionAlmacenamiento()) {
                                usoLocalizacion.RequestPermissions("Sin el permiso de grabación no puedo acceder a su ubicación y alamacenarla", SOLICITUD_PERMISO_LOCALIZACION);
                            }
                            if (!usoMicrofono.hayPermisoGrabacionAlmacenamiento()) {
                                usoMicrofono.RequestPermissions("Sin el permiso de grabación no puedo empezar a grabar", SOLICITUD_PERMISO_GRABACION);
                            }
                        }
                    }
                }else if((Medios.contains("SMS"))&& !(Medios.contains("Telegram"))) {
                    if ((micro == false) && (drive.equals("Nada"))) { /**Llamada + SMS**/
                        Log.e(TAG_Panico, "Llamada + SMS");
                        if ((ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) && (ContextCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS) == PackageManager.PERMISSION_GRANTED)) {
                            lanzarPantallaPanico(null);
                            UsoContacto.llamarTelefono(contacto);
                            servSendSMS(pos);
                        } else {
                            if (ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                solicitarPermiso(Manifest.permission.CALL_PHONE, "Sin el permiso el administrador de llamadas no podrá realizar la llamada.", CALL_PHONE, this);
                            }
                            if (ContextCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED) {
                                solicitarPermiso(Manifest.permission.SEND_SMS, "Sin el permiso no se podrá enviar SMS.", SEND_SMS, this);
                            }
                        }
                    } else if ((micro == true) && (drive.equals("Nada"))) {  /**Llamada + SMS + Micro**/
                        Log.e(TAG_Panico, "Llamada + SMS + Micro");
                        if ((ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) && (ContextCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS) == PackageManager.PERMISSION_GRANTED) && (usoMicrofono.hayPermisoGrabacionAlmacenamiento())) {
                            lanzarPantallaPanico(null);
                            UsoContacto.llamarTelefono(contacto);
                            servSendSMS(pos);
                            int start = 0;
                            int stop = 1;
                            timer.schedule(new MicroRepeat(start), 0, 29000);
                        } else {
                            if (ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                solicitarPermiso(Manifest.permission.CALL_PHONE, "Sin el permiso el administrador de llamadas no podrá realizar la llamada.", CALL_PHONE, this);
                            }
                            if (ContextCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED) {
                                solicitarPermiso(Manifest.permission.SEND_SMS, "Sin el permiso no se podrá enviar SMS.", SEND_SMS, this);
                            }
                            if (!usoMicrofono.hayPermisoGrabacionAlmacenamiento()) {
                                usoMicrofono.RequestPermissions("Sin el permiso de grabación no puedo empezar a grabar", SOLICITUD_PERMISO_GRABACION);
                            }
                        }
                    } else if ((micro == false) && (!drive.equals("Nada"))) {  /**Llamada + SMS + Drive**/
                        Log.e(TAG_Panico, "Llamada + SMS + Drive");
                        if ((ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) && (ContextCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS) == PackageManager.PERMISSION_GRANTED) && (usoLocalizacion.hayPermisoLocalizacionAlmacenamiento())) {
                            lanzarPantallaPanico(null);
                            UsoContacto.llamarTelefono(contacto);
                            servSendSMS(pos);
                           // new CasosUsoDrive(this, mGoogleApiClient).execute();
                            timer.schedule(new DriveRepeat(this, mGoogleApiClient), 0, 30000);
                        } else {
                            if (ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                solicitarPermiso(Manifest.permission.CALL_PHONE, "Sin el permiso el administrador de llamadas no podrá realizar la llamada.", CALL_PHONE, this);
                            }
                            if (ContextCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED) {
                                solicitarPermiso(Manifest.permission.SEND_SMS, "Sin el permiso no se podrá enviar SMS.", SEND_SMS, this);
                            }
                            if (!usoLocalizacion.hayPermisoLocalizacionAlmacenamiento()) {
                                usoLocalizacion.RequestPermissions("Sin el permiso de grabación no puedo acceder a su ubicación y alamacenarla", SOLICITUD_PERMISO_LOCALIZACION);
                            }
                        }
                    } else if ((micro == true) && (!drive.equals("Nada"))) { /**Llamada + SMS + Micro + Drive**/
                        Log.e(TAG_Panico, "Llamada + SMS + Micro + Drive");
                        if ((ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) && (usoLocalizacion.hayPermisoLocalizacionAlmacenamiento()) && (ContextCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS) == PackageManager.PERMISSION_GRANTED) && (usoMicrofono.hayPermisoGrabacionAlmacenamiento())) {
                            lanzarPantallaPanico(null);
                            UsoContacto.llamarTelefono(contacto);
                            servSendSMS(pos);
                            int start = 0;
                            int stop = 1;
                            timer.schedule(new MicroRepeat(start), 0, 29000);
                            timer.schedule(new DriveRepeat(this, mGoogleApiClient), 31000, 31000);
                        } else {
                            if (ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                solicitarPermiso(Manifest.permission.CALL_PHONE, "Sin el permiso el administrador de llamadas no podrá realizar la llamada.", CALL_PHONE, this);
                            }
                            if (ContextCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED) {
                                solicitarPermiso(Manifest.permission.SEND_SMS, "Sin el permiso no se podrá enviar SMS.", SEND_SMS, this);
                            }
                            if (!usoLocalizacion.hayPermisoLocalizacionAlmacenamiento()) {
                                usoLocalizacion.RequestPermissions("Sin el permiso de grabación no puedo acceder a su ubicación y alamacenarla", SOLICITUD_PERMISO_LOCALIZACION);
                            }
                            if (!usoMicrofono.hayPermisoGrabacionAlmacenamiento()) {
                                usoMicrofono.RequestPermissions("Sin el permiso de grabación no puedo empezar a grabar", SOLICITUD_PERMISO_GRABACION);
                            }
                        }
                    }
                }else if((Medios.contains("SMS"))&& (Medios.contains("Telegram"))) {
                    if ((micro == false) && (drive.equals("Nada"))) { /**Llamada + SMS + Telegram**/
                        Log.e(TAG_Panico, "Llamada + SMS + Telegram");
                        if ((ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) && (ContextCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS) == PackageManager.PERMISSION_GRANTED)) {
                            lanzarPantallaPanico(null);
                            UsoContacto.llamarTelefono(contacto);
                            servSendSMS(pos);
                            new CasosUsoTelegram(this, usoLocalizacion).execute();
                        } else {
                            if (ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                solicitarPermiso(Manifest.permission.CALL_PHONE, "Sin el permiso el administrador de llamadas no podrá realizar la llamada.", CALL_PHONE, this);
                            }
                            if (ContextCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED) {
                                solicitarPermiso(Manifest.permission.SEND_SMS, "Sin el permiso no se podrá enviar SMS.", SEND_SMS, this);
                            }
                        }
                    } else if ((micro == true) && (drive.equals("Nada"))) {  /**Llamada + Telegram + SMS + Micro**/
                        Log.e(TAG_Panico, "Llamada + SMS + Telegram + Micro");
                        if ((ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) && (ContextCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS) == PackageManager.PERMISSION_GRANTED) && (usoMicrofono.hayPermisoGrabacionAlmacenamiento())) {
                            lanzarPantallaPanico(null);
                            UsoContacto.llamarTelefono(contacto);
                            servSendSMS(pos);
                            new CasosUsoTelegram(this, usoLocalizacion).execute();
                            int start = 0;
                            int stop = 1;
                            timer.schedule(new MicroRepeat(start), 0, 29000);
                        } else {
                            if (ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                solicitarPermiso(Manifest.permission.CALL_PHONE, "Sin el permiso el administrador de llamadas no podrá realizar la llamada.", CALL_PHONE, this);
                            }
                            if (ContextCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED) {
                                solicitarPermiso(Manifest.permission.SEND_SMS, "Sin el permiso no se podrá enviar SMS.", SEND_SMS, this);
                            }
                            if (!usoMicrofono.hayPermisoGrabacionAlmacenamiento()) {
                                usoMicrofono.RequestPermissions("Sin el permiso de grabación no puedo empezar a grabar", SOLICITUD_PERMISO_GRABACION);
                            }
                        }
                    } else if ((micro == false) && (!drive.equals("Nada"))) {  /**Llamada + Telegram + SMS + Drive**/
                        Log.e(TAG_Panico, "Llamada + SMS + Telegram + Drive");
                        if ((ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) && (ContextCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS) == PackageManager.PERMISSION_GRANTED) && (usoLocalizacion.hayPermisoLocalizacionAlmacenamiento())) {
                            lanzarPantallaPanico(null);
                            UsoContacto.llamarTelefono(contacto);
                            servSendSMS(pos);
                            new CasosUsoTelegram(this, usoLocalizacion).execute();
                            //new CasosUsoDrive(this, mGoogleApiClient).execute();
                            timer.schedule(new DriveRepeat(this, mGoogleApiClient), 0, 30000);
                        } else {
                            if (ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                solicitarPermiso(Manifest.permission.CALL_PHONE, "Sin el permiso el administrador de llamadas no podrá realizar la llamada.", CALL_PHONE, this);
                            }
                            if (ContextCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED) {
                                solicitarPermiso(Manifest.permission.SEND_SMS, "Sin el permiso no se podrá enviar SMS.", SEND_SMS, this);
                            }
                            if (!usoLocalizacion.hayPermisoLocalizacionAlmacenamiento()) {
                                usoLocalizacion.RequestPermissions("Sin el permiso de grabación no puedo acceder a su ubicación y alamacenarla", SOLICITUD_PERMISO_LOCALIZACION);
                            }

                        }
                    } else if ((micro == true) && (!drive.equals("Nada"))) { /**Llamada + SMS + Telegram + Micro + Drive**/
                        Log.e(TAG_Panico, "Llamada + SMS + Telegram + Micro + Drive");
                        if ((ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) && (usoLocalizacion.hayPermisoLocalizacionAlmacenamiento()) && (ContextCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS) == PackageManager.PERMISSION_GRANTED) && (usoMicrofono.hayPermisoGrabacionAlmacenamiento())) {
                            lanzarPantallaPanico(null);
                            UsoContacto.llamarTelefono(contacto);
                            servSendSMS(pos);
                            new CasosUsoTelegram(this, usoLocalizacion).execute();
                            int start = 0;
                            int stop = 1;
                            timer.schedule(new MicroRepeat(start), 0, 29000);
                            timer.schedule(new DriveRepeat(this, mGoogleApiClient), 31000, 31000);
                        } else {
                            if (ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                solicitarPermiso(Manifest.permission.CALL_PHONE, "Sin el permiso el administrador de llamadas no podrá realizar la llamada.", CALL_PHONE, this);
                            }
                            if (ContextCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED) {
                                solicitarPermiso(Manifest.permission.SEND_SMS, "Sin el permiso no se podrá enviar SMS.", SEND_SMS, this);
                            }
                            if (!usoLocalizacion.hayPermisoLocalizacionAlmacenamiento()) {
                                usoLocalizacion.RequestPermissions("Sin el permiso de grabación no puedo acceder a su ubicación y alamacenarla", SOLICITUD_PERMISO_LOCALIZACION);
                            }
                            if (!usoMicrofono.hayPermisoGrabacionAlmacenamiento()) {
                                usoMicrofono.RequestPermissions("Sin el permiso de grabación no puedo empezar a grabar", SOLICITUD_PERMISO_GRABACION);
                            }
                        }
                    }
                }
            }else{
                new AlertDialog.Builder(this)
                        .setTitle("Botón Pánico")
                        .setMessage("No tienes ningún contacto en tu lista para realizar esta operación. Añade contactos a tu lista y configúralo como desees. Para más información pulsa en 'Acerca de...'")
                        .show();
            }
        }else if(Medios.contains("SMS")){
            if (ContactIsNull(contacto, pos) == false) {
                if(!(Medios.contains("Llamada"))&& !(Medios.contains("Telegram"))) {
                    if ((micro == false) && (drive.equals("Nada"))) { /**Solo SMS**/
                        Log.e(TAG_Panico, "SMS");
                        if (ContextCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS) == PackageManager.PERMISSION_GRANTED) {
                            lanzarPantallaPanico(null);
                            servSendSMS(pos);
                            //new Background_SMS(pos, contactos, adaptador).execute();
                        } else {
                            if (ContextCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED) {
                                solicitarPermiso(Manifest.permission.SEND_SMS, "Sin el permiso no se podrá enviar SMS.", SEND_SMS, this);
                            }
                        }
                    } else if ((micro == true) && (drive.equals("Nada"))) {  /**SMS + Micro**/
                        Log.e(TAG_Panico, "SMS + Micro");
                        if ((ContextCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS) == PackageManager.PERMISSION_GRANTED) && (usoMicrofono.hayPermisoGrabacionAlmacenamiento())) {
                            lanzarPantallaPanico(null);
                            servSendSMS(pos);
                            int start = 0;
                            int stop = 1;
                            timer.schedule(new MicroRepeat(start), 0, 29000);
                        } else {
                            if (ContextCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED) {
                                solicitarPermiso(Manifest.permission.SEND_SMS, "Sin el permiso no se podrá enviar SMS.", SEND_SMS, this);
                            }
                            if (!usoMicrofono.hayPermisoGrabacionAlmacenamiento()) {
                                usoMicrofono.RequestPermissions("Sin el permiso de grabación no puedo empezar a grabar", SOLICITUD_PERMISO_GRABACION);
                            }
                        }
                    } else if ((micro == false) && (!drive.equals("Nada"))) {  /**SMS + Drive**/
                        Log.e(TAG_Panico, "SMS + Drive");
                        if ((ContextCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS) == PackageManager.PERMISSION_GRANTED) && (usoLocalizacion.hayPermisoLocalizacionAlmacenamiento())) {
                            lanzarPantallaPanico(null);
                            servSendSMS(pos);
                            //new CasosUsoDrive(this, mGoogleApiClient).execute();
                            timer.schedule(new DriveRepeat(this, mGoogleApiClient), 0, 30000);
                        } else {
                            if (ContextCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED) {
                                solicitarPermiso(Manifest.permission.SEND_SMS, "Sin el permiso no se podrá enviar SMS.", SEND_SMS, this);
                            }
                            if (!usoLocalizacion.hayPermisoLocalizacionAlmacenamiento()) {
                                usoLocalizacion.RequestPermissions("Sin el permiso de grabación no puedo acceder a su ubicación y alamacenarla", SOLICITUD_PERMISO_LOCALIZACION);
                            }
                        }
                    } else if ((micro == true) && (!drive.equals("Nada"))) { /**SMS + Micro + Drive**/
                        Log.e(TAG_Panico, "SMS + Micro + Drive");
                        if ((usoLocalizacion.hayPermisoLocalizacionAlmacenamiento()) && (ContextCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS) == PackageManager.PERMISSION_GRANTED) && (usoMicrofono.hayPermisoGrabacionAlmacenamiento())) {
                            lanzarPantallaPanico(null);
                            servSendSMS(pos);
                            int start = 0;
                            int stop = 1;
                            timer.schedule(new MicroRepeat(start), 0, 29000);
                            timer.schedule(new DriveRepeat(this, mGoogleApiClient), 31000, 31000);
                        } else {
                            if (ContextCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED) {
                                solicitarPermiso(Manifest.permission.SEND_SMS, "Sin el permiso no se podrá enviar SMS.", SEND_SMS, this);
                            }
                            if (!usoLocalizacion.hayPermisoLocalizacionAlmacenamiento()) {
                                usoLocalizacion.RequestPermissions("Sin el permiso de grabación no puedo acceder a su ubicación y alamacenarla", SOLICITUD_PERMISO_LOCALIZACION);
                            }
                            if (!usoMicrofono.hayPermisoGrabacionAlmacenamiento()) {
                                usoMicrofono.RequestPermissions("Sin el permiso de grabación no puedo empezar a grabar", SOLICITUD_PERMISO_GRABACION);
                            }
                        }
                    }
                }else if (!(Medios.contains("Llamada"))&& (Medios.contains("Telegram"))) {
                    if ((micro == false) && (drive.equals("Nada"))) { /**Telegram + SMS**/
                        Log.e(TAG_Panico, "SMS + Telegram");
                        if (ContextCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS) == PackageManager.PERMISSION_GRANTED) {
                            lanzarPantallaPanico(null);
                            new CasosUsoTelegram(this, usoLocalizacion).execute();
                            servSendSMS(pos);
                        } else {
                            if (ContextCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED) {
                            solicitarPermiso(Manifest.permission.SEND_SMS, "Sin el permiso no se podrá enviar SMS.", SEND_SMS, this);
                        }
                    }
                    } else if ((micro == true) && (drive.equals("Nada"))) {  /**Telegram + SMS + Micro**/
                        Log.e(TAG_Panico, "SMS + Telegram + Micro");
                        if ((ContextCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS) == PackageManager.PERMISSION_GRANTED) && (usoMicrofono.hayPermisoGrabacionAlmacenamiento())) {
                            lanzarPantallaPanico(null);
                            servSendSMS(pos);
                            //new Background_Microfono().execute();
                            //timer.schedule(new MicroRepeat(), 0, 29000);
                            int start = 0;
                            int stop = 1;
                            timer.schedule(new MicroRepeat(start), 0, 29000);
                            new CasosUsoTelegram(this, usoLocalizacion).execute();
                        } else {
                            if (ContextCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED) {
                                solicitarPermiso(Manifest.permission.SEND_SMS, "Sin el permiso no se podrá enviar SMS.", SEND_SMS, this);
                            }
                            if (!usoMicrofono.hayPermisoGrabacionAlmacenamiento()) {
                                usoMicrofono.RequestPermissions("Sin el permiso de grabación no puedo empezar a grabar", SOLICITUD_PERMISO_GRABACION);
                            }
                        }
                    } else if ((micro == false) && (!drive.equals("Nada"))) {  /**Telegram + SMS + Drive**/
                        Log.e(TAG_Panico, "SMS + Telegram + Drive");
                        if ((ContextCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS) == PackageManager.PERMISSION_GRANTED) && (usoLocalizacion.hayPermisoLocalizacionAlmacenamiento())) {
                            lanzarPantallaPanico(null);
                            servSendSMS(pos);
                            //new CasosUsoDrive(this, mGoogleApiClient).execute();
                            timer.schedule(new DriveRepeat(this, mGoogleApiClient), 0, 30000);
                            new CasosUsoTelegram(this, usoLocalizacion).execute();
                        } else {
                            if (ContextCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED) {
                                solicitarPermiso(Manifest.permission.SEND_SMS, "Sin el permiso no se podrá enviar SMS.", SEND_SMS, this);
                            }
                            if (!usoLocalizacion.hayPermisoLocalizacionAlmacenamiento()) {
                                usoLocalizacion.RequestPermissions("Sin el permiso de grabación no puedo acceder a su ubicación y alamacenarla", SOLICITUD_PERMISO_LOCALIZACION);
                            }
                        }
                    } else if ((micro == true) && (!drive.equals("Nada"))) { /**Telegram + SMS + Micro + Drive**/
                        Log.e(TAG_Panico, "SMS + Telegram + Micro + Drive");
                        if ((usoLocalizacion.hayPermisoLocalizacionAlmacenamiento()) && (ContextCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS) == PackageManager.PERMISSION_GRANTED) && (usoMicrofono.hayPermisoGrabacionAlmacenamiento())) {
                            lanzarPantallaPanico(null);
                            servSendSMS(pos);
                            //new Background_SMS(pos, contactos, adaptador).execute();
                            int start = 0;
                            int stop = 1;
                            timer.schedule(new MicroRepeat(start), 0, 29000);
                            timer.schedule(new DriveRepeat(this, mGoogleApiClient), 31000, 31000);
                            new CasosUsoTelegram(this, usoLocalizacion).execute();
                        } else {
                            if (ContextCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED) {
                                solicitarPermiso(Manifest.permission.SEND_SMS, "Sin el permiso no se podrá enviar SMS.", SEND_SMS, this);
                            }
                            if (!usoLocalizacion.hayPermisoLocalizacionAlmacenamiento()) {
                                usoLocalizacion.RequestPermissions("Sin el permiso de grabación no puedo acceder a su ubicación y alamacenarla", SOLICITUD_PERMISO_LOCALIZACION);
                            }
                            if (!usoMicrofono.hayPermisoGrabacionAlmacenamiento()) {
                                usoMicrofono.RequestPermissions("Sin el permiso de grabación no puedo empezar a grabar", SOLICITUD_PERMISO_GRABACION);
                            }
                        }
                    }
                }
            }else{
                new AlertDialog.Builder(this)
                        .setTitle("Botón Pánico")
                        .setMessage("No tienes ningún contacto en tu lista para realizar esta operación. Añade contactos a tu lista y configúralo como desees. Para más información pulsa en 'Acerca de...'")
                        .show();
            }
        }else if (Medios.contains("Telegram")){
            if (!(Medios.contains("Llamada"))&& !(Medios.contains("SMS"))) {
                if ((micro == false) && (drive.equals("Nada"))) { /**Telegram**/
                    Log.e(TAG_Panico, "Telegram");
                    lanzarPantallaPanico(null);
                    new CasosUsoTelegram(this, usoLocalizacion).execute();
                } else if ((micro == true) && (drive.equals("Nada"))) {  /**Telegram + Micro**/
                    Log.e(TAG_Panico, "Telegram +  Micro");
                    if (usoMicrofono.hayPermisoGrabacionAlmacenamiento()) {
                        lanzarPantallaPanico(null);
                        int start = 0;
                        int stop = 1;
                        timer.schedule(new MicroRepeat(start), 0, 29000);
                        new CasosUsoTelegram(this, usoLocalizacion).execute();
                    } else {
                        if (!usoMicrofono.hayPermisoGrabacionAlmacenamiento()) {
                            usoMicrofono.RequestPermissions("Sin el permiso de grabación no puedo empezar a grabar", SOLICITUD_PERMISO_GRABACION);
                        }
                    }
                } else if ((micro == false) && (!drive.equals("Nada"))) {  /**Telegram + Drive**/
                    Log.e(TAG_Panico, "Telegram +  Drive");
                    if (usoLocalizacion.hayPermisoLocalizacionAlmacenamiento()) {
                        lanzarPantallaPanico(null);
                        timer.schedule(new DriveRepeat(this, mGoogleApiClient), 0, 30000);
                        //new CasosUsoDrive(this, mGoogleApiClient).execute();
                        new CasosUsoTelegram(this, usoLocalizacion).execute();
                    } else {
                        if (!usoLocalizacion.hayPermisoLocalizacionAlmacenamiento()) {
                            usoLocalizacion.RequestPermissions("Sin el permiso de grabación no puedo acceder a su ubicación y alamacenarla", SOLICITUD_PERMISO_LOCALIZACION);
                        }
                    }
                } else if ((micro == true) && (!drive.equals("Nada"))) { /**Telegram + Micro + Drive**/
                    Log.e(TAG_Panico, "Telegram +  Micro +  Drive");
                    if ((usoLocalizacion.hayPermisoLocalizacionAlmacenamiento()) && (usoMicrofono.hayPermisoGrabacionAlmacenamiento())) {
                        lanzarPantallaPanico(null);
                        int start = 0;
                        int stop = 1;
                        timer.schedule(new MicroRepeat(start), 0, 29000);
                        timer.schedule(new DriveRepeat(this, mGoogleApiClient), 31000, 31000);
                        new CasosUsoTelegram(this, usoLocalizacion).execute();
                    } else {
                        if (!usoLocalizacion.hayPermisoLocalizacionAlmacenamiento()) {
                            usoLocalizacion.RequestPermissions("Sin el permiso de grabación no puedo acceder a su ubicación y alamacenarla", SOLICITUD_PERMISO_LOCALIZACION);
                        }
                        if (!usoMicrofono.hayPermisoGrabacionAlmacenamiento()) {
                            usoMicrofono.RequestPermissions("Sin el permiso de grabación no puedo empezar a grabar", SOLICITUD_PERMISO_GRABACION);
                        }
                    }
                }
            }
        }
    }


    public void servSendSMS(int pos) {
        Intent serv = new Intent(this, Servicio_SMS.class);
        serv.putExtra("pos", pos);
        this.startService(serv);
    }

    public void servMicro() {
        Intent serv = new Intent(this, Servicio_Micro.class);
        this.startService(serv);
    }


    public void llamarTelefono(View view) {
        UsoContacto.llamarTelefono(contacto);
    }

    // Solicitud de permisos

    public static void solicitarPermiso(final String permiso, String justificacion, final int requestCode, final Activity actividad) {
        if (ActivityCompat.shouldShowRequestPermissionRationale(actividad, permiso)) {
            new AlertDialog.Builder(actividad)
                    .setTitle("Solicitud de permiso")
                    .setMessage(justificacion)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            ActivityCompat.requestPermissions(actividad, new String[]{permiso}, requestCode);
                        }
                    })
                    .show();
        } else {
            ActivityCompat.requestPermissions(actividad, new String[]{permiso}, requestCode);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == CALL_PHONE) {
            if ((grantResults.length == 1) && (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                //UsoContacto.PermisoObtenidoPhone(contacto);
                llamarTelefono(vista);
            } else {
                Toast.makeText(this, "Sin el permiso, no puedo realizar la " +
                        "acción", Toast.LENGTH_SHORT).show();
            }
        } else if (requestCode == READ_CONTACTS) {
            if ((grantResults.length == 1) && (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                //EditarContactoActivity.AccessContact();
                PermisoObtenido = 1;
                PermisoObtenido();
                //UsoContacto.PermisoObtenidoContactos();
                //EditarContactoActivity.permisoConcedido(contacto);
            } else {
                //PermisoObtenido = 0;
                //PermisoObtenido();
                Toast.makeText(this, "Sin el permiso, no puedo realizar la " +
                        "acción", Toast.LENGTH_SHORT).show();
            }
        } else if (requestCode == SOLICITUD_PERMISO_LOCALIZACION) {
            /*if ((grantResults.length == 1) && (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                usoLocalizacion.permisoConcedido();
            } else {
                Toast.makeText(this, "Sin el permiso, no puedo realizar la " +
                        "acción", Toast.LENGTH_SHORT).show();
            }*/
            if (grantResults.length > 0) {
                boolean permissionToRecord = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                boolean permissionToStore = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                if (permissionToRecord && permissionToStore) {
                    Toast.makeText(getApplicationContext(), "Permiso Obtenido", Toast.LENGTH_LONG).show();
                    usoLocalizacion.permisoConcedido();
                } else {
                    Toast.makeText(this, "Sin el permiso, no puedo realizar la " +
                            "acción", Toast.LENGTH_SHORT).show();
                }
            }
        } else if (requestCode == SEND_SMS) {
            if ((grantResults.length == 1) && (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                servSendSMS(pos);
            } else {
                Toast.makeText(this, "Sin el permiso, no puedo realizar la " +
                        "acción", Toast.LENGTH_SHORT).show();
            }
        } else if (requestCode == SOLICITUD_PERMISO_GRABACION) {
            if (grantResults.length > 0) {
                boolean permissionToRecord = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                boolean permissionToStore = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                if (permissionToRecord && permissionToStore) {
                    Toast.makeText(getApplicationContext(), "Permiso Obtenido", Toast.LENGTH_LONG).show();
                    usoMicrofono.permisoConcedido();
                } else {
                    Toast.makeText(this, "Sin el permiso, no puedo realizar la " +
                            "acción", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    public void PermisoObtenido() {
        EditarContactoActivity.PermisoObtenido(PermisoObtenido);
    }


    public boolean ContactIsNull(Contacto contacto, int pos) {
        contacto = adaptador.contactoPosicion(pos); //que me devuelve el lugar en esa posición(id) ///por defecto el primero... habrá que ver si puedo hacer un bucle con todos o buscar uno específico
        if ((contacto.getNombre() == "ERROR") && (contacto.getEstado() == "ERROR") && (contacto.getContacto_id() == "ERROR") && (contacto.getTelefono() == 0) && (contacto.getFecha() == 0)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * GOOGLE ASSISTANT
     **/
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case RECOGNIZE_SPEECH_ACTIVITY:
                if (resultCode == RESULT_OK && null != data) {
                    ArrayList<String> speech = data
                            .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    String strSpeech2Text = speech.get(0);
                    String palabra_clave = preferencias_dueña.getString("Palabra_clave", "ayuda");
                    if (palabra_clave.equals("")){
                        palabra_clave = "ayuda";
                    }
                    if (strSpeech2Text.equals(palabra_clave)) {
                        lanzarPanico(vista);
                        //DrivePulsado = 1;
                    }

                    //grabar.setText(strSpeech2Text);
                }
                break;
            case REQUEST_CODE_RESOLUTION:
                if (resultCode == RESULT_OK) {
                    Log.e("Prueba_GoogleApiClient", "Resultado recibido_googleApiClient");
                    mGoogleApiClient.connect();
                }
                break;
        }
    }

    public void porVoz() {
        Intent intentActionRecognizeSpeech = new Intent(
                RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        // Configura el Lenguaje (Español-España) o Inglés (Los dos idiomas de esta aplicación)
        intentActionRecognizeSpeech.putExtra(
                RecognizerIntent.EXTRA_LANGUAGE_MODEL, "es-esp");  ////hacer opciones inglés/español??? o que me coja el idioma del movil como con los strings
        try {
            startActivityForResult(intentActionRecognizeSpeech,
                    RECOGNIZE_SPEECH_ACTIVITY);
        } catch (ActivityNotFoundException a) {
            Toast.makeText(getApplicationContext(),
                    "Tú dispositivo no soporta el reconocimiento por voz",
                    Toast.LENGTH_SHORT).show();
        }
    }

    public void initGoogleAssistant() {
        if (preferencias_dueña.getBoolean("Por_voz", false)) {
            porVoz();
        }
    }

    /**ESTADOS DE LA ACTIVIDAD**/

    @Override
    protected void onStart() {
        super.onStart();
        //Toast.makeText(this, "onStart", Toast.LENGTH_SHORT).show();

        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this).addApi(Drive.API)
                    .addScope(Drive.SCOPE_FILE)
                    .addScope(Drive.SCOPE_APPFOLDER) // required for App Folder sample
                    .addConnectionCallbacks(usoDrive)
                    .addOnConnectionFailedListener(usoDrive)
                    .build();
        }
        mGoogleApiClient.connect();
    }

    @Override
    protected void onResume() {
        super.onResume();
        usoLocalizacion.activar();
        //initGoogleAssistant();
        //Toast.makeText(this, "onResume", Toast.LENGTH_SHORT).show();

        //drive
        //lanzarDrive();


        //drive2
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this).addApi(Drive.API)
                    .addScope(Drive.SCOPE_FILE)
                    .addScope(Drive.SCOPE_APPFOLDER) // required for App Folder sample
                    .addConnectionCallbacks(usoDrive)
                    .addOnConnectionFailedListener(usoDrive)
                    .build();
        }
        mGoogleApiClient.connect();

    }

    @Override
    protected void onPause() {
        //Toast.makeText(this, "onPause", Toast.LENGTH_SHORT).show();
        //usoLocalizacion.desactivar(); //Para ahorrar batería

        //drive
        /*if (mGoogleApiClient != null) {
            mGoogleApiClient.disconnect();
            //DrivePulsado =0;
        }*/
        super.onPause();

        //drive2
        /*if (mGoogleApiClient_main != null) {
            mGoogleApiClient_main.disconnect();
        }
        super.onPause();*/
    }

    @Override
    protected void onStop() {
        //Toast.makeText(this, "onStop", Toast.LENGTH_SHORT).show();
        super.onStop();
    }

    @Override
    protected void onRestart() {
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this).addApi(Drive.API)
                    .addScope(Drive.SCOPE_FILE)
                    .addScope(Drive.SCOPE_APPFOLDER) // required for App Folder sample
                    .addConnectionCallbacks(usoDrive)
                    .addOnConnectionFailedListener(usoDrive)
                    .build();
        }
        mGoogleApiClient.connect();

        initGoogleAssistant();

        super.onRestart();
        //Toast.makeText(this, "onRestart", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onDestroy() {
        //Toast.makeText(this, "onDestroy", Toast.LENGTH_SHORT).show();
        usoLocalizacion.desactivar(); //Para ahorrar batería
        if (mGoogleApiClient != null) {
            mGoogleApiClient.disconnect();
        }
        if (preferencias_dueña.getBoolean("microfono", false)) {
            usoMicrofono.PararGrabacion(); //Para ahorrar batería
        }
        super.onDestroy();

    }


}



