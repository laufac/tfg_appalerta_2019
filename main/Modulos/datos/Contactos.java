package com.tfg.laura.appalerta.datos;

import com.tfg.laura.appalerta.modelo.Contacto;

public interface Contactos {
    Contacto contacto(int id); //Devuelve el contacto dado su id
    int nuevo(); //Añade un contacto en blanco y devuelve su id
    void borrar(int id); //Elimina el contacto con el id indicado
    int size(); //Devuelve el número de contactos
    void actualiza(int id, Contacto contacto); //Reemplaza un elemento
    boolean BuscaContactoId(String id);
    boolean BuscaContactotelf(String telf);
}

/* Una clase que implemente esta interface va a almacenar una lista
de objetos de tipo Contacto.
 Mediante los métodos indicados vamos a poder acceder y modificar esta lista.*/
