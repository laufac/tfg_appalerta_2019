package com.tfg.laura.appalerta.datos;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.preference.PreferenceManager;

import com.tfg.laura.appalerta.modelo.Contacto;
import com.tfg.laura.appalerta.presentacion.Aplicacion;

import java.math.BigDecimal;
import java.math.BigInteger;

public class ContactosBD extends SQLiteOpenHelper implements Contactos {

    Context contexto;

    //Constructor llama al constructor heredado
    public ContactosBD(Context contexto) {
        super(contexto, "contactos", null, 1);
        this.contexto = contexto;
    }

    //Parámetros:

    /**contexto: Contexto usado para abrir o crear la base de datos.

     nombre: Nombre de la base de datos que se creará. En nuestro caso, “puntuaciones”.

     cursor: Se utiliza para crear un objeto de tipo cursor. En nuestro caso no lo necesitamos.

     version: Número de versión de la base de datos empezando desde 1. En el caso de que la base de
     datos actual tenga una versión más antigua se llamará a onUpgrade() para que actualice la base
     de datos.*/


    /**En la aplicación necesitamos solo la tabla contactos, que es creada por medio del comando
     * SQL CREATE TABLE contactos… La primera columna tiene por nombre _id y será un entero usado como
     * clave principal. Su valor será introducido automáticamente por el sistema, de forma que dos
     * filas no tengan nunca el mismo valor de _id.**/
    @Override public void onCreate(SQLiteDatabase bd) {
        bd.execSQL("CREATE TABLE contactos ("+
                "_id INTEGER PRIMARY KEY AUTOINCREMENT, "+
                "nombre TEXT, " +
                "estado TEXT, " +
                "contacto_id TEXT, " +
                "fecha BIGINT, " +
                "telefono BIGINT)");
    }

    /**Ha de quedar claro que este constructor solo creará una base de datos (llamando a onCreate())
     *  siestá todavía no existe. Si ya fue creada en una ejecución anterior, nos devolverá la base
     *  de datos existente.**/


    /**El método onUpgrade() está vacío. Si más adelante, en una segunda versión de la aplicación,
     * decidiéramos crear una nueva estructura para la base de datos, tendríamos que indicar un
     * número de versión superior, por ejemplo la 2. Cuando se ejecute el código sobre un sistema
     * que disponga de una base de datos con la versión 1, se invocará el método onUpgrade(). En él
     * tendremos que escribir los comandos necesarios para transformar la antigua base de datos en
     * la nueva, tratando de conservar la información de la versión anterior.**/
    @Override public void onUpgrade(SQLiteDatabase db, int oldVersion,
                                    int newVersion) {
        /*db.execSQL("DELETE FROM contactos WHERE _id = 0");
        db.execSQL("DELETE FROM contactos WHERE _id = 1");
        */
    }

    //Buscar el lugar correspondiente a un id y devolverlo
    @Override
    public Contacto contacto(int id) {
        Cursor cursor = getReadableDatabase().rawQuery(
                "SELECT * FROM contactos WHERE _id = "+id, null);
        try {
            if (cursor.moveToNext())
                return obtieneContacto(cursor);
            else
                throw new SQLException("Error al acceder al elemento _id = "+id);
        } catch (Exception e) {
            throw e;
        } finally {
            if (cursor!=null) cursor.close(); //Es importante cerrar lo antes posibles el consumo de memoria.
        }
    }

    @Override
    public int nuevo() {
        int _id = -1;
        Contacto contacto = new Contacto();
        /**Si consultas el constructor de la clase, observarás que solo se inicializan posicion,
         * tipo y fecha. El resto de los valores serán una cadena vacía para String y 0 para valores
         * numéricos. Acto seguido, se crea una nueva fila con esta información. Los valores de
         * texto y numéricos tampoco se indican, al inicializarse de la misma manera.
         El método ha de devolver el _id del elemento añadido. Para conseguirlo se realiza una
         consulta buscando una fila con la misma fecha que acabamos de introducir.**/
        getWritableDatabase().execSQL("INSERT INTO contactos (nombre, estado, " +
                "fecha, telefono) VALUES ('', 'Nuevo' , "+ contacto.getFecha() + " ,'')");
        Cursor c = getReadableDatabase().rawQuery(
                "SELECT _id FROM contactos WHERE fecha = " + contacto.getFecha(), null);
        if (c.moveToNext()) _id = c.getInt(0);
        c.close();
        return _id;
    }

    @Override
    public void borrar(int id) {
        getWritableDatabase().execSQL("DELETE FROM contactos WHERE _id = " + id);
    }

    @Override
    public int size() {
        int size = 0;

        // SQL statement
        String sql = "SELECT COUNT(*) FROM  contactos ";

        //run query
        Cursor cursor = getReadableDatabase().rawQuery(sql, null);

        //did i get a result?
        if(cursor.getCount() > 0 ){
            cursor.moveToFirst();
            size = cursor.getInt(0);
        }

        cursor.close();

        return size;
    }


    //Su finalidad es reemplazar el lugar correspondiente al id indicado por un nuevo lugar.
    @Override
    public void actualiza(int id, Contacto contacto) {
        getWritableDatabase().execSQL("UPDATE contactos SET" +
                "   nombre = '" + contacto.getNombre() +
                "', estado = '" + contacto.getEstado() +
                "', contacto_id = '" + contacto.getContacto_id() +
                "', fecha = " + contacto.getFecha() +
                " , telefono = " + contacto.getTelefono() +
                " WHERE _id = " + id);
    }



    //crea un nuevo lugar con los datos de la posición actual de un Cursor.
    public static Contacto obtieneContacto(Cursor cursor) {
        Contacto contacto = new Contacto();
        if(cursor.getCount() != 0){
            contacto.setNombre(cursor.getString(1));
            contacto.setEstado(cursor.getString(2));
            contacto.setContacto_id(cursor.getString(3));
            contacto.setFecha(cursor.getLong(4));
            contacto.setTelefono(cursor.getLong(5));

        }else{
            contacto.setNombre("ERROR");
            contacto.setEstado("ERROR");
            contacto.setContacto_id("ERROR");
            contacto.setFecha(0);
            contacto.setTelefono(0);
        }

        return contacto;
    }


    // devuelve un cursor que contiene todos los datos de la tabla.
    public Cursor extraeCursor() {
        String consulta = "SELECT * FROM contactos";
        SQLiteDatabase bd = getReadableDatabase();
        return bd.rawQuery(consulta, null);
    }


    public boolean BuscaContactoId(String id) {
        Cursor cursor = getReadableDatabase().rawQuery(
                "SELECT * FROM contactos WHERE contacto_id = " + id, null);
        if (cursor.moveToNext()){
            return true;
        }else{
            return false;
        }
    }

    public boolean BuscaContactotelf(String telf) { //para encontrar un repetido escrito a mano
        Cursor cursor = getReadableDatabase().rawQuery(
                "SELECT * FROM contactos WHERE telefono = " + telf, null);
        if (cursor.moveToNext()){
            return true;
        }else{
            return false;
        }
    }

    public boolean BuscaId(int id) {
        Cursor cursor = getReadableDatabase().rawQuery(
                "SELECT * FROM contactos WHERE _id = " + id, null);
        if (cursor.moveToNext()){
            return true;
        }else{
            return false;
        }
    }




}




