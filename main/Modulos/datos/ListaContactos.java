package com.tfg.laura.appalerta.datos;

import com.tfg.laura.appalerta.modelo.Contacto;

import java.util.ArrayList;
import java.util.List;

public class ListaContactos implements Contactos {

    protected List<Contacto> contactoList = ejemploContactos();

    public ListaContactos() {
        contactoList = ejemploContactos();
    }

    public Contacto contacto(int id) {
        return contactoList.get(id);
    }

    public void anyade(Contacto contacto) {
        contactoList.add(contacto);
    }

    public int nuevo() {
        Contacto contacto = new Contacto();
        contactoList.add(contacto);
        return contactoList.size() - 1;
    }

    public void borrar(int id) {
        contactoList.remove(id);
    }

    public int size() {
        return contactoList.size();
    }

    public void actualiza(int id, Contacto contacto) {
        contactoList.set(id, contacto);
    }


    public static ArrayList<Contacto> ejemploContactos() {
        ArrayList<Contacto> contactos = new ArrayList<Contacto>();


        contactos.add(new Contacto("Lols Martinez","Nuevo", " ", +45502532656L));
        contactos.add(new Contacto("Ana Fernández","Nuevo","", +3462533638L));
        return contactos;
    }
    public boolean BuscaContactoId(String id){
        return true;
    }
    public boolean BuscaContactotelf(String telf){
        return true;
    }
}

